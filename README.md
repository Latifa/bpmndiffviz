## Main information

**BPMNDiffViz** - a web based application for comparing business process models in BPMN 2.0 XML format.

BPMNDiffViz was developed as a tool, which helps users to find structural differences between two business processes represented in BPMN format. The differences are visualized and the user can navigate between them. BPMNDiffViz shows, which elements match (blue color), which should be added (green color), which should be deleted (red color) to transform one process model to another.

![BPMN.jpg](https://bitbucket.org/repo/699g5X/images/3279316693-BPMN.jpg)

## Get sources
```
$ git clone https://sivanov68@bitbucket.org/sivanov68/bpmndiffviz.git
```

## Installation
1. Install Tomcat (tested with version 7.0.54) - https://tomcat.apache.org/download-70.cgi
1. Install PostgreSQL (tested with version 9.1.1) - http://www.postgresql.org/download/
1. Get sources of BPMNDiffViz
1. Create database "vkr"
1. Create folder for storing models (*R_FOLDER*)
1. Change last line in SQL script (*BPMNDiffViz\src\main\resources\queries*): VALUES (TRUE, '*PATH_TO_R_FOLDER*');
1. Run SQL script for database "vkr" (*BPMNDiffViz\src\main\resources\queries*)
1. Set up database connection (*BPMNDiffViz\src\main\webapp\jdbc.properties*)
1. Get BPMNDiffViz\target\BPMNDiffViz.war or build BPMNDiffViz.war by yourself using Maven (*run maven task: package*)
1. Put BPMNDiffViz.war to Tomcat webapps folder (Tomcat7.0.54\webapps)
1. Start Tomcat using shell (Tomcat7.0.54\bin\startup.bat)
1. Open browser and get URL: http://localhost:8080/BPMNDiffViz/

## Website
[BPMNDiffViz web page](http://pais.hse.ru/research/projects/CompBPMN)

## License
Apache License Version 2.0

## Contacts
Sergey Y.Ivanov (syuivanov@gmail.com)  
Anna A.Kalenkova (akalenkova@hse.ru)