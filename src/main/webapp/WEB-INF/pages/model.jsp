<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>BPMN Comparator by PAIS Lab</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/morris-0.4.3.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom-styles.css"/>"/>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/jquery-1.10.2.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bpmn/bpmn-navigated-viewer.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/morris/morris.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/raphael/raphael-2.1.0.min.js"/>"></script>
    <script src="<c:url value="/resources/js/my/model.js"/>"></script>

</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value="/"/>">BPMNDiffViz<b> BY PAIS LAB</b></a>
        </div>

    </nav>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li>
                    <a href="<c:url value="/"/>"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a href="<c:url value="/comparison/first_step"/>"><i class="fa fa-link"></i> New comparison</a>
                </li>
                <li>
                    <a href="<c:url value="/uploadModel"/>"><i class="fa fa-cloud-upload"></i> New model</a>
                </li>
                <li>
                    <a class="active-menu" href="<c:url value="/models"/>"><i class="fa fa-sitemap"></i> Models</a>
                </li>
                <li>
                    <a href="<c:url value="/results"/>"><i class="fa fa-tasks"></i> Results</a>
                </li>
            </ul>

        </div>

    </nav>

    <div id="page-wrapper">
        <div id="page-inner">


            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header">
                        Model Viewer
                        <small></small>
                    </h1>
                </div>
            </div>

            <div class="panel-body">
                <ul class="nav nav-tabs" id="nav">
                    <li class="">
                        <a href="#Diagram" data-toggle="tab" aria-expanded="true">
                            Model diagram
                        </a>
                    </li>
                    <li class="active">
                        <a href="#Profile" data-toggle="tab" aria-expanded="true">
                            Model profile
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade" id="Diagram">
                        <div class="canvas" id="canvas">
                        </div>
                    </div>
                    <div class="tab-pane fade active in" id="Profile">
                        <div class="canvas">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="alert alert-info text-center" role="alert"><h3 class="margin-top-0">
                                        Information</h3></div>
                                    <h3>Model name: <span style="font-weight: bold">${model.name}</span></h3>
                                    <br/>

                                    <h3>File name: <span style="font-weight: bold">${model.document.name}</span>
                                    </h3>
                                    <br/>

                                    <h3>Uploaded date: <span style="font-weight: bold">${model.document.uploadDate}</span>
                                    </h3>

                                </div>
                                <div class="col-md-8">
                                    <div class="alert alert-info text-center" role="alert"><h3 class="margin-top-0">
                                        Statistics</h3></div>
                                    <div class="text-center" id="morris-donut-chart"></div>
                                    <div style="text-align: right"><span style="color: red">*</span> Only key elements
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" name="id" id="id" value="${model.id}">

            <input type="hidden" id="taskCount" value="${taskCount}">
            <input type="hidden" id="sequenceFlowCount" value="${sequenceFlowCount}">
            <input type="hidden" id="startEventCount" value="${startEventCount}">
            <input type="hidden" id="endEventCount" value="${endEventCount}">
            <input type="hidden" id="exclusiveGatewayCount" value="${exclusiveGatewayCount}">
            <input type="hidden" id="parallelGatewayCount" value="${parallelGatewayCount}">
            <input type="hidden" id="processCount" value="${processCount}">
            <input type="hidden" id="messageFlowCount" value="${messageFlowCount}">
            <footer><p class="margin-bottom-0 padding-top-0">All right reserved. Laboratory of Process-Aware
                Information
                Systems (PAIS
                Lab).</p>
            </footer>
        </div>
    </div>
</div>

</body>

</html>
