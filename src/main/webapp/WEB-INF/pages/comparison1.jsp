<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>BPMN Comparator by PAIS Lab</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom-styles.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/dataTables.bootstrap.css"/>"/>
    <script type="text/javascript" src="<c:url value="/resources/js/bpmn/jquery.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/dataTables/jquery.dataTables.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/dataTables/dataTables.bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/my/comparison1.js"/>"></script>

</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value="/"/>">BPMNDiffViz<b> BY PAIS LAB</b></a>
        </div>

    </nav>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li>
                    <a href="<c:url value="/"/>"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a class="active-menu" href="<c:url value="/comparison/first_step"/>"><i class="fa fa-link"></i> New
                        comparison</a>
                </li>
                <li>
                    <a href="<c:url value="/uploadModel"/>"><i class="fa fa-cloud-upload"></i> New model</a>
                </li>
                <li>
                    <a href="<c:url value="/models"/>"><i class="fa fa-sitemap"></i> Models</a>
                </li>
                <li>
                    <a href="<c:url value="/results"/>"><i class="fa fa-tasks"></i> Results</a>
                </li>
            </ul>

        </div>

    </nav>

    <div id="page-wrapper">
        <div id="page-inner">


            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header">
                        First step: Select first model
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="margin-bottom-0" action="<c:url value="/comparison/second_step"/>"
                                  method="post">
                                <input name="firstModelID" id="firstModelID" type="hidden"/>
                                <table class="table table-bordered" id="dataTable">
                                    <thead>
                                    <tr>
                                        <th style="width: 14px;"></th>
                                        <th>Model id</th>
                                        <th>Model name</th>
                                        <th>Upload date</th>
                                        <th>Origin file name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${modelList}" var="model">
                                        <tr>
                                            <td><input class="checkbox" type="checkbox"/></td>
                                            <td>${model.id}</td>
                                            <td><a href="<c:url value="/model?id=${model.id}"/>"
                                                   target="_blank">${model.name}</a></td>
                                            <td>${model.document.uploadDate}</td>
                                            <td>${model.document.name}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <button id="nextStep" class="btn btn-success btn-lg" style="width: 222px">Next step
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer><p class="margin-bottom-0 padding-top-0">All right reserved. Laboratory of Process-Aware Information
            Systems (PAIS
            Lab).</p>
        </footer>
    </div>
</div>

</body>

</html>
