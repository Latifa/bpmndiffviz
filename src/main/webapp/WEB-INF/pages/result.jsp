<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="fmtBean" scope="page" class="ru.pais.vkr.utils.FormaterBean"/>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>BPMN Comparator by PAIS Lab</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/morris-0.4.3.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom-styles.css"/>"/>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/jquery-1.10.2.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bpmn/bpmn-navigated-viewer.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/morris/morris.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/raphael/raphael-2.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/charts/easypiechart.js"/>"></script>
    <script src="<c:url value="/resources/js/my/result.js"/>"></script>
</head>

<body>
<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value="/"/>">BPMNDiffViz<b> BY PAIS LAB</b></a>
        </div>
    </nav>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li>
                    <a href="<c:url value="/"/>"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a href="<c:url value="/comparison/first_step"/>"><i class="fa fa-link"></i> New
                        comparison</a>
                </li>
                <li>
                    <a href="<c:url value="/uploadModel"/>"><i class="fa fa-cloud-upload"></i> New model</a>
                </li>
                <li>
                    <a href="<c:url value="/models"/>"><i class="fa fa-sitemap"></i> Models</a>
                </li>
                <li>
                    <a class="active-menu" href="<c:url value="/results"/>"><i class="fa fa-tasks"></i> Results</a>
                </li>
            </ul>

        </div>
    </nav>

    <div id="page-wrapper">
        <div id="page-inner">

            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header">
                        Comparison results
                        <small></small>
                    </h1>
                </div>
            </div>

            <div class="panel-body">
                <ul class="nav nav-tabs" id="nav">
                    <li class="active">
                        <a href="#Results" data-toggle="tab" aria-expanded="true">
                            Results
                        </a>
                    </li>
                    <li class="">
                        <a href="#Statistics" data-toggle="tab" aria-expanded="true">
                            Statistics
                        </a>
                    </li>
                    <li class="">
                        <a href="#Settings" data-toggle="tab" aria-expanded="true">
                            Settings
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade active in" id="Results">
                        <div class="row">
                            <div class="col-md-8 padding-right-0">
                                <div class="canvasParent">
                                    Model: <span style="font-weight: bold">${result.firstName}</span>

                                    <div class="modelCanvas" id="canvas1"></div>
                                    <br/>

                                    Model: <span style="font-weight: bold">${result.secondName}</span>

                                    <div class="modelCanvas" id="canvas2"></div>
                                </div>
                                <div class="checkbox margin-bottom-0">
                                    <label>
                                        <input id="showAllMarkers" type="checkbox"> Show all markers
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4 padding-left-0">
                                <div class="canvasParent">


                                    Final score: <span style="font-weight: bold">
                                    ${fmtBean.format(result.penalty)}</span>
                                    <c:forEach items="${result.deletedSet}" var="i">
                                        <div class="cursorPointer alert alert-danger margin-bottom-3 margin-top-3 padding-5">
                                            <span class="my-white-badge-danger"> ${fmtBean.format(i.differentLabel)}</span> ${i.definition}
                                            <input class="del" type="hidden" value="${i.firstID}">
                                        </div>
                                    </c:forEach>
                                    <c:forEach items="${result.addedSet}" var="i">
                                        <div class="cursorPointer alert alert-success margin-bottom-3 margin-top-3 padding-5">
                                            <span class="my-white-badge-success">${fmtBean.format(i.differentLabel)}</span> ${i.definition}
                                            <input class="add" type="hidden" value="${i.firstID}">
                                        </div>
                                    </c:forEach>
                                    <c:forEach items="${result.matchingSet}" var="i">
                                        <div class="cursorPointer alert alert-info margin-bottom-3 margin-top-3 padding-5">
                                            <span class="my-white-badge-info"> ${fmtBean.format(i.differentLabel)}</span>
                                                ${i.definition}
                                            <c:if test="${i.differentContainer != 0}">
                                                + <span
                                                    class="my-white-badge-container"> ${i.differentContainer}</span> (different containers)
                                            </c:if>
                                            <input class="match1" type="hidden"
                                                   value="${i.firstID}">
                                            <input class="match2" type="hidden"
                                                   value="${i.secondID}">
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="Statistics">
                        <div class="canvas">
                            <div class="row">
                                <div class="col-md-3">
                                    <h3>Final score: <span style="font-weight: bold">${fmtBean.format(result.penalty)}</span>
                                    </h3>

                                    <h3>Recursion depth: <span style="font-weight: bold">${result.recCounter - 1}</span>
                                    </h3>
                                </div>

                                <div class="col-md-3 text-center">
                                    <h4 style="text-align: center">Matched elements</h4>

                                    <h3><span
                                            style="font-weight: bold">${result.matchingSet.size()}</span>
                                    </h3>

                                    <div class="easypiechart" style="text-align: center" id="easypiechart-teal"
                                         data-percent="${percent2}"><span
                                            class="percent">${percent2}%</span>
                                    </div>
                                </div>

                                <div class="col-md-3 text-center">
                                    <h4 style="text-align: center">Deleted elements</h4>

                                    <h3><span
                                            style="font-weight: bold">${result.deletedSet.size()}</span>
                                    </h3>

                                    <div class="easypiechart" style="text-align: center" id="easypiechart-orange"
                                         data-percent="${percent3}"><span
                                            class="percent">${percent3}%</span>
                                    </div>
                                </div>

                                <div class="col-md-3 text-center">
                                    <h4 style="text-align: center">Added elements</h4>

                                    <h3><span
                                            style="font-weight: bold">${result.addedSet.size()}</span>
                                    </h3>

                                    <div class="easypiechart" style="text-align: center" id="easypiechart-red"
                                         data-percent="${percent4}"><span
                                            class="percent">${percent4}%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="Settings">
                        <div class="col-md-12 padding-left-0 padding-right-0">
                            <div class="canvasParent">

                                <div class="row form-inline margin-0 padding-bottom-5">
                                    <strong>Coefficient for label
                                        comparison (Levenshtein distance)</strong>
                                    <input name="Ldistance" type="text"
                                           class="form-control"
                                           style="width:70px;" value="${result.factorLevenshtein}" disabled>
                                </div>

                                <div class="panel-group margin-bottom-0">
                                    <c:forEach items="${result.costConfig.getAllConfigs()}" var="i" varStatus="loop">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse${loop.index}" class=""
                                                       aria-expanded="true">${i.toString().replaceFirst("ru.pais.vkr.comparator.config.", "").replaceFirst("Config.*", "")}</a>
                                                </h4>
                                            </div>
                                            <div id="collapse${loop.index}" class="panel-collapse collapse"
                                                 aria-expanded="true">
                                                <div class="panel-body">
                                                    <form class="config margin-bottom-0 form-horizontal">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 my-label ">Insert</label>

                                                            <div class="col-sm-3">
                                                                <input name="insert" type="text"
                                                                       class="form-control"
                                                                       value="${i.insert}" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 my-label ">Delete</label>

                                                            <div class="col-sm-3">
                                                                <input name="delete" type="text"
                                                                       class="form-control"
                                                                       value="${i.delete}" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 my-label ">Different
                                                                container</label>

                                                            <div class="col-sm-3">
                                                                <input name="diff" type="text" class="form-control"
                                                                       value="${i.diffprocess}" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 my-label ">Change label</label>

                                                            <div class="col-sm-3">
                                                                <input class="form-control"
                                                                       value="Levenshtein distance"
                                                                       disabled>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id1" id="id1" value="${result.firstID}">
            <input type="hidden" name="id1" id="id2" value="${result.secondID}">
            <input type="hidden" name="root" id="root" value="${pageContext.request.contextPath}">

            <footer class="padding-top-0"><p class="margin-bottom-0 padding-top-0">All right reserved. Laboratory of
                Process-Aware
                Information
                Systems (PAIS
                Lab).</p>
            </footer>
        </div>
    </div>
</div>

</body>

</html>
