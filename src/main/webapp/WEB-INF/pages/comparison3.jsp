<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>BPMN Comparator by PAIS Lab</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom-styles.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/dataTables.bootstrap.css"/>"/>
    <script type="text/javascript" src="<c:url value="/resources/js/bpmn/jquery.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/jquery.form.min.js"/>"></script>
    <script src="<c:url value="/resources/js/my/comparison3.js"/>"></script>

</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value="/"/>">BPMNDiffViz<b> BY PAIS LAB</b></a>
        </div>

    </nav>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li>
                    <a href="<c:url value="/"/>"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a class="active-menu" href="<c:url value="/comparison/first_step"/>"><i class="fa fa-link"></i> New
                        comparison</a>
                </li>
                <li>
                    <a href="<c:url value="/uploadModel"/>"><i class="fa fa-cloud-upload"></i> New model</a>
                </li>
                <li>
                    <a href="<c:url value="/models"/>"><i class="fa fa-sitemap"></i> Models</a>
                </li>
                <li>
                    <a href="<c:url value="/results"/>"><i class="fa fa-tasks"></i> Results</a>
                </li>
            </ul>

        </div>

    </nav>

    <div id="page-wrapper">
        <div id="page-inner">


            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header">
                        Third step: Cost of operations
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 padding-0">
                                <div class="panel-body" style="padding-bottom: 0; padding-top: 0">
                                    <div class="alert alert-success">
                                        <strong>First model:</strong> <a
                                            href="<c:url value="/model?id=${firstModel.id}"/>"
                                            target="_blank">${firstModel.name}</a>
                                        <br/>
                                        <strong>Second model:</strong> <a
                                            href="<c:url value="/model?id=${secondModel.id}"/>"
                                            target="_blank">${secondModel.name}</a>
                                    </div>
                                    <form action="<c:url value="/comparison/second_step"/>"
                                          style="display: inline-block">
                                        <input name="firstModelID" type="hidden" value="${firstModel.id}"/>
                                        <input name="secondModelID" type="hidden"
                                               value="${secondModel.id}"/>
                                        <button class="btn btn-danger btn-lg" style="width: 222px">Previous step
                                        </button>
                                    </form>
                                    <form id="fourth_step" action="<c:url value="/comparison/fourth_step"/>"
                                          style="display: inline-block" method="post">
                                        <input name="firstModelID" type="hidden" value="${firstModel.id}"/>
                                        <input name="secondModelID" type="hidden" value="${secondModel.id}"/>
                                        <button id="nextStep" class="btn btn-success btn-lg" style="width: 222px">Final
                                            step
                                        </button>
                                        <input name="firstModelID" type="hidden" value="${firstModel.id}"/>
                                    </form>

                                    <div class="row padding-5 form-inline">
                                        <strong>Coefficient for label
                                            comparison (Levenshtein distance)</strong>
                                        <input name="Ldistance" type="text"
                                               class="form-control"
                                               style="width:70px;" value="1.00">
                                    </div>
                                </div>

                                <div class="panel-group margin-bottom-0">
                                    <input id="formSave" type="hidden" value="<c:url value="/saveConfig"/>"/>

                                    <c:forEach items="${configs}" var="i" varStatus="loop">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse${loop.index}" class=""
                                                       aria-expanded="true">${i.toString().replaceFirst("ru.pais.vkr.comparator.config.", "").replaceFirst("Config.*", "")}</a>
                                                </h4>
                                            </div>
                                            <div id="collapse${loop.index}" class="panel-collapse collapse"
                                                 aria-expanded="true">
                                                <div class="panel-body">
                                                    <form class="config margin-bottom-0 form-horizontal"
                                                          action="<c:url value="/saveConfig"/>"
                                                          method="post">
                                                        <input name="config" type="hidden" value="${i}">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 my-label ">Insert</label>

                                                            <div class="col-sm-3">
                                                                <input name="insert" type="text"
                                                                       class="form-control"
                                                                       value="${i.insert}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 my-label ">Delete</label>

                                                            <div class="col-sm-3">
                                                                <input name="delete" type="text"
                                                                       class="form-control"
                                                                       value="${i.delete}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 my-label ">Different
                                                                container</label>

                                                            <div class="col-sm-3">
                                                                <input name="diff" type="text" class="form-control"
                                                                       value="${i.diffprocess}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 my-label ">Change label</label>

                                                            <div class="col-sm-3">
                                                                <input class="form-control"
                                                                       value="Levenshtein distance"
                                                                       disabled>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary"
                                                                style="width: 100px">Save
                                                        </button>

                                                        <h4 id="${i.toString().replaceFirst("ru.pais.vkr.comparator.config.", "").replaceFirst("Config.*", "")}"
                                                            style="display:inline-block"
                                                            class="myConfig margin-bottom-0"><span
                                                                class="label label-success">Successfully saved</span>
                                                        </h4>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer><p class="margin-bottom-0 padding-top-0">All right reserved. Laboratory of Process-Aware Information
        Systems (PAIS
        Lab).</p>
    </footer>
</div>
</div>

</body>

</html>
