<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>BPMN Comparator by PAIS Lab</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom-styles.css"/>"/>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/jquery-1.10.2.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
</head>
<body>

<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value="/"/>">BPMNDiffViz<b> BY PAIS LAB</b></a>
        </div>

    </nav>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li>
                    <a href="<c:url value="/"/>"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a href="<c:url value="/comparison/first_step"/>"><i class="fa fa-link"></i> New comparison</a>
                </li>
                <li>
                    <a href="<c:url value="/uploadModel"/>"><i class="fa fa-cloud-upload"></i> New model</a>
                </li>
                <li>
                    <a href="<c:url value="/models"/>"><i class="fa fa-sitemap"></i> Models</a>
                </li>
                <li>
                    <a href="<c:url value="/results"/>"><i class="fa fa-tasks"></i> Results</a>
                </li>
            </ul>

        </div>

    </nav>

    <div id="page-wrapper">
        <div id="page-inner">

            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header">
                        System failure
                        <small></small>
                    </h1>
                </div>
            </div>

            <div class="row">

                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-body padding-top-5">
                                        -------------------------------------------------------------------------------------------------------------------------
                                        URL: ${url}
                                        <br/>
                                        Exception: ${exp}
                                        <br/>
                                        Message: ${errorMessage}
                                        <br/>
                                        -------------------------------------------------------------------------------------------------------------------------
                                        <br/>
                                        Please, contact <a href="mailto:syuivanov@gmail.com">syuivanov@gmail.com</a> and
                                        send time, models, and list of actions
                                        for diagnostic this error.
                                        <br/>
                                        Thanks, PAIS Team.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer><p class="margin-bottom-0 padding-top-0">All right reserved. Laboratory of Process-Aware Information
            Systems (PAIS
            Lab).
        </p>
        </footer>
    </div>
</div>

</body>

</html>
