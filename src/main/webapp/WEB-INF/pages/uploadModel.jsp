<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>BPMN Comparator by PAIS Lab</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom-styles.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/formValidation.min.css"/>"/>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/jquery-1.10.2.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/validation/formValidation.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/validation/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap/bootstrap-filestyle.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/my/uploadModel.js"/>"></script>
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value="/"/>">BPMNDiffViz<b> BY PAIS LAB</b></a>
        </div>

    </nav>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li>
                    <a href="<c:url value="/"/>"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a href="<c:url value="/comparison/first_step"/>"><i class="fa fa-link"></i> New comparison</a>
                </li>
                <li>
                    <a class="active-menu" href="<c:url value="/uploadModel"/>"><i class="fa fa-cloud-upload"></i> New model</a>
                </li>
                <li>
                    <a href="<c:url value="/models"/>"><i class="fa fa-sitemap"></i> Models</a>
                </li>
                <li>
                    <a href="<c:url value="/results"/>"><i class="fa fa-tasks"></i> Results</a>
                </li>
            </ul>

        </div>

    </nav>

    <div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header">
                        Upload model
                        <small>BPMN 2.0 (.bpmn)</small>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form enctype="multipart/form-data"
                                          action="<c:url value="/saveModel"/>" method="post" id="uploadForm">
                                        <div class="form-group">
                                            <label>Model name</label>
                                            <input name="name" class="form-control" placeholder="BPMN 2.0 model">
                                        </div>
                                        <div class="form-group">
                                            <label>File input (.bpmn)</label>
                                            <input type="file" class="filestyle" data-buttonBefore="true" name="file"
                                                   accept=".bpmn">
                                        </div>
                                        <button id="uploadModelSubmit" type="submit" class="btn btn-default"><i
                                                class="fa fa-plus-circle"></i>
                                            Submit
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <c:if test="${id ne null && id ne 0}">
                                <div class="alert alert-success margin-bottom-0">
                                        ${message} Open in viewer: <a href="<c:url value="/model?id=${id}"/>">${modelName}</a>
                                </div>
                            </c:if>
                            <c:if test="${id eq null}">
                                <div class="alert alert-danger margin-bottom-0">
                                        ${message}
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer><p class="margin-bottom-0 padding-top-0">All right reserved. Laboratory of Process-Aware Information Systems (PAIS
            Lab).
        </p>
        </footer>
    </div>
</div>

</body>

</html>
</html>
