<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="fmtBean" scope="page" class="ru.pais.vkr.utils.FormaterBean"/>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>BPMN Comparator by PAIS Lab</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/custom-styles.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/dataTables.bootstrap.css"/>"/>
    <script type="text/javascript" src="<c:url value="/resources/js/bpmn/jquery.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/dataTables/jquery.dataTables.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/dataTables/dataTables.bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/my/results.js"/>"></script>

</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value="/"/>">BPMNDiffViz<b> BY PAIS LAB</b></a>
        </div>

    </nav>

    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li>
                    <a href="<c:url value="/"/>"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a href="<c:url value="/comparison/first_step"/>"><i class="fa fa-link"></i> New
                        comparison</a>
                </li>
                <li>
                    <a href="<c:url value="/uploadModel"/>"><i class="fa fa-cloud-upload"></i> New model</a>
                </li>
                <li>
                    <a href="<c:url value="/models"/>"><i class="fa fa-sitemap"></i> Models</a>
                </li>
                <li>
                    <a class="active-menu" href="<c:url value="/results"/>"><i class="fa fa-tasks"></i> Results</a>
                </li>
            </ul>

        </div>

    </nav>

    <div id="page-wrapper">
        <div id="page-inner">


            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header">
                        Results of comparisons
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <input name="firstModelID" id="firstModelID" type="hidden"/>
                            <table class="table table-bordered" id="dataTable">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>First model</th>
                                    <th>Second model</th>
                                    <th>Costs</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${results}" var="i">
                                    <tr>
                                        <td><a href="<c:url value="/result?id=${i.id}"/>"><fmt:formatDate
                                                value="${i.date}" pattern="yyyy-MM-dd HH:mm:ss"/></a></td>
                                        <td><a href="<c:url value="/model?id=${i.firstID}"/>"
                                               target="_blank">${i.firstName}</a></td>
                                        <td><a href="<c:url value="/model?id=${i.secondID}"/>"
                                               target="_blank">${i.secondName}</a></td>
                                        <td>${fmtBean.format(i.penalty)}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer><p class="margin-bottom-0 padding-top-0">All right reserved. Laboratory of Process-Aware Information
            Systems (PAIS
            Lab).</p>
        </footer>
    </div>
</div>

</body>

</html>
