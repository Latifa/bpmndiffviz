$(document).ready(function () {

    $('#uploadForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'The name is required'
                    },
                    stringLength: {
                        min: 5,
                        max: 30,
                        message: 'The name must be more than 5 and less than 30 characters long'
                    }
                }
            },
            file: {
                validators: {
                    notEmpty: {
                        message: 'The file with extension .bpmn is required'
                    }
                }
            }
        }
    });

    $('#uploadModelSubmit').submit();

});
