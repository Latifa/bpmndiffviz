$(document).ready(function () {
    var t = $('#dataTable').dataTable({
        "aoColumnDefs": [
            {'bSortable': false, 'aTargets': [0]}
        ]
    });

    $("#nextStep").attr('disabled', 'disabled');

    t.on('change', 'tbody .checkbox', function (e) {
        t.$('input:checked').prop('checked', false);
        $(this).prop('checked', true);
        t.$('tr.selected-model').removeClass('selected-model');
        $(this).addClass('selected-model');
        $(this).parent().parent().addClass('selected-model');
        $("#nextStep").removeAttr('disabled');
        $("#firstModelID").val($(this).parent().next().html())
    });

});
