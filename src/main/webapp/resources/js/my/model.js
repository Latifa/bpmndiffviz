$(document).ready(function () {

    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Sequence Flow",
            value: parseInt($("#sequenceFlowCount").val())
        }, {
            label: "Start Event",
            value: parseInt($("#startEventCount").val())
        }, {
            label: "End Event",
            value: parseInt($("#endEventCount").val())
        }, {
            label: "Exclusive Gateway",
            value: parseInt($("#exclusiveGatewayCount").val())
        }, {
            label: "Parallel Gateway",
            value: parseInt($("#parallelGatewayCount").val())
        }, {
            label: "Process",
            value: parseInt($("#processCount").val())
        }, {
            label: "Task",
            value: parseInt($("#taskCount").val())
        }],
        colors: [
            '#83d0c9', '#fb5858',
            '#cccccc', '#cc1c31',
            '#ff5500', '#ffec00',
            '#a4d89b', '#649046'
        ],
        resize: true
    });

    var BpmnViewer = window.BpmnJS;

    // create viewer
    var bpmnViewer = new BpmnViewer({
        container: '#canvas'
    });

    // import function
    function importXML(xml) {

        // import diagram
        bpmnViewer.importXML(xml, function (err) {

            var canvas = bpmnViewer.get('canvas');
            var overlays = bpmnViewer.get('overlays');

            // attach an overlay to a node
            //overlays.add('sid-52EB1772-F36E-433E-8F5B-D5DFD26E6F26', 'note', {
            //position: {
            //    top: -5,
            //    left: -5
            //},
            //html: '<div style="width: 10px; background: fuchsia; color: white;">0</div>'
            //});

            // add marker
            //canvas.addMarker('sid-52EB1772-F36E-433E-8F5B-D5DFD26E6F26', 'myClassDelete');

            // add marker
            //canvas.addMarker('sid-E49425CF-8287-4798-B622-D2A7D78EF00B', 'myClassAdd');

            // add marker
            //canvas.addMarker('sid-7B791A11-2F2E-4D80-AFB3-91A02CF2B4FD', 'myClassAdd');

        });
    }

    // import bpmn from server
    $.get('resources/model?id=' + $("#id").val(), importXML, 'text');

    // change some classes
    $("#nav li:eq(1)").removeClass("active");
    $("#nav li:eq(0)").addClass("active");
    $('#Profile').removeClass("active in");
    $('#Diagram').addClass("active in");

});
