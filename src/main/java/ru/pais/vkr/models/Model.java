package ru.pais.vkr.models;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class Model {

    private long id;
    private Document document;
    private String name;

    public Model() {
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
