package ru.pais.vkr.models;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class Storage {

    private long id;
    private boolean isAvailable;
    private String path;

    public Storage() {
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
