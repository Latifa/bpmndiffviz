package ru.pais.vkr.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

public class BPMNUtils {

    public static boolean isFlowElement(Class aClass) {
        return aClass.getSimpleName().equals("Association") ||
                aClass.getSimpleName().equals("ConversationLink") ||
                aClass.getSimpleName().equals("MessageFlow") ||
                aClass.getSimpleName().equals("SequenceFlow");
    }
}
