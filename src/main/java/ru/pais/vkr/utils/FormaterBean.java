package ru.pais.vkr.utils;

import java.text.MessageFormat;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 * Date: 30.08.2015
 * Time: 19:05
 */
public class FormaterBean {

    public String format(String number) {
        MessageFormat mf = new MessageFormat("{0,number,#.##}", new Locale( "en_US"));
        return mf.format(new Double[] { new Double(number) });
    }

}
