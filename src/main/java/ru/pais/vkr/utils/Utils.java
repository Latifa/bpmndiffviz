package ru.pais.vkr.utils;

import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.comparator.entities.MatchingPair;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class Utils {

    public static boolean compareList(List<Class> matchingPairs1, List<Class> matchingPairs2) {
        List<Class> matchingPairsn1 = new ArrayList<>(matchingPairs1);
        List<Class> matchingPairsn2 = new ArrayList<>(matchingPairs2);
        if(matchingPairsn1.size() == 0 && matchingPairsn2.size() == 0) {
            return true;
        }
        if(matchingPairsn1.size() != matchingPairsn2.size()) {
            return false;
        }

        matchingPairsn1.stream().forEach(x -> {
            Optional<Class> t = matchingPairsn2.stream().filter(z -> z.equals(x)).findFirst();
            if(t.isPresent()) {
                matchingPairsn2.remove(t.get());
            }
        });
        return matchingPairsn2.size() == 0;
    }

    public static boolean compareSet(Set<MatchingPair> matchingPairs1, Set<MatchingPair> matchingPairs2) {
        Set<MatchingPair> matchingPairsn1 = new HashSet<>(matchingPairs1);
        Set<MatchingPair> matchingPairsn2 = new HashSet<>(matchingPairs2);
        if(matchingPairsn1.size() == 0 && matchingPairsn2.size() == 0) {
            return true;
        }
        if(matchingPairsn1.size() != matchingPairsn2.size()) {
            return false;
        }

        matchingPairsn1.stream().forEach(x -> {
            Optional<MatchingPair> t = matchingPairsn2.stream().filter(z -> z.getFirst().equals(x.getFirst()) && z.getSecond().equals(x.getSecond())).findFirst();
            if(t.isPresent()) {
                matchingPairsn2.remove(t.get());
            }
        });

        return matchingPairsn2.size() == 0;
    }

}
