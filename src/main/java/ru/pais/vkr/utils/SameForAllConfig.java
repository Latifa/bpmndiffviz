package ru.pais.vkr.utils;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.xml.ModelInstance;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import ru.pais.vkr.comparator.entities.ComparisonResult;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class SameForAllConfig {

    public static boolean sameContainer(ModelElementInstance modelElementInstance1, ModelElementInstance modelElementInstance2, ComparisonResult comparisonResult) {
        boolean needsCompareParent = false;

        String name1 = modelElementInstance1.getParentElement().getElementType().getTypeName();
        String name2 = modelElementInstance2.getParentElement().getElementType().getTypeName();

        if (name1.equals("process") &&
                name2.equals("process")) {
            needsCompareParent = true;
        }
        if (name1.equals("lane") &&
                name2.equals("lane")) {
            needsCompareParent = true;
        }
        if (name1.equals("subProcess") &&
                name2.equals("subProcess")) {
            needsCompareParent = true;
        }
        if (needsCompareParent) {
            ModelElementInstance modelElementInstanceFromMap = comparisonResult.getValueByKeyFromMatchingMap(modelElementInstance1.getParentElement());
            if (modelElementInstanceFromMap != modelElementInstance2.getParentElement()) {
                return true;
            }
        }
        return false;
    }
}
