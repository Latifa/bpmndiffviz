package ru.pais.vkr.utils;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.soap.Node;
import java.io.File;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

//Класс для работы с файлами в системе
public class FileManager {
    private static volatile FileManager instance;

    private FileManager() {
    }

    //Класс Singleton
    public static FileManager getInstance() {
        FileManager localInstance = instance;
        if (localInstance == null) {
            synchronized (FileManager.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new FileManager();
                }
            }
        }
        return localInstance;
    }

    /**
     * Метод возвращает все названия файлов для папки и вложенных в нее
     *
     * @param folder папка, содержащая файлы
     * @param list   набор всех названий файлов для рекурсии
     * @return набор всех названий файлов
     */
    public List<String> listFilesForFolder(final File folder, List<String> list) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, list);
            } else {
                list.add(fileEntry.getAbsolutePath());
            }
        }
        return list;
    }

    /**
     * Метод возвращает полный путь к файлу с именем файла и расширрением
     *
     * @param toFolder путь до папки
     * @param s        имя файла
     * @return полный путь к файлу
     */
    public String getPathWithFolderAndFileNameWithExtensionTXT(String toFolder, String s) {
        StringBuilder stringBuilderTXT = new StringBuilder(s);
        stringBuilderTXT.reverse();
        stringBuilderTXT = new StringBuilder(stringBuilderTXT.toString().replaceFirst("lmx", "txt"));
        stringBuilderTXT.reverse();
        s = stringBuilderTXT.toString();

        int last = s.lastIndexOf('\\');
        StringBuilder stringBuilder = new StringBuilder(toFolder);
        stringBuilder.append("\\");
        stringBuilder.append(s.substring(++last, s.length()));
        return stringBuilder.toString();
    }

    /**
     * Метод возвращает полный путь к файлу с именем файла и расширением
     *
     * @param toFolder путь до папки
     * @param s        имя файла
     * @return полный путь к файлу
     */
    public String getPathWithFolderAndFileName(String toFolder, String s) {
        int last = s.lastIndexOf('\\');
        StringBuilder stringBuilder = new StringBuilder(toFolder);
        stringBuilder.append("\\");
        stringBuilder.append(s.substring(++last, s.length()));
        return stringBuilder.toString();
    }

    /**
     * Копирование файлов в директорию
     *
     * @param filesNamesStorage список адресов файлов для копирования
     * @param folderAddress     папка, в которую происходит копирование
     * @return копирование прошло удачно или нет
     */
    public boolean copyFilesToFolder(List<String> filesNamesStorage, String folderAddress) {
        try {
            File dir = new File(folderAddress);
            //Если данной папки не существует, она создается
            if (!dir.exists()) {
                dir.createNewFile();
            }
            //Если это не папка
            if (!dir.isDirectory()) {
                return false;
            }
            //Копирование
            for (String s : filesNamesStorage) {
                FileUtils.copyFileToDirectory(new File(s), dir);
            }
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    /**
     * Встречается или нет BPMN элемент в файле с таким названием
     *
     * @param bpmnElementName имя BPMN элемента
     * @param element         элемент для рекурсивного анализа
     * @return найден или нет BPMN элемент в XML файле
     */
    public boolean findBPMNElementInFile(String bpmnElementName, Element element) {
        boolean find = false;
        if (element.getTagName().equals(bpmnElementName)) {
            find = true;
        }
        //Проход по всем вложенным тегам
        NodeList nodeList = element.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                find = find || findBPMNElementInFile(bpmnElementName, (Element) nodeList.item(i));
            }
        }
        return find;
    }
}
