package ru.pais.vkr.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.pais.vkr.dao.DocumentDAO;
import ru.pais.vkr.dao.ModelDAO;
import ru.pais.vkr.dao.StorageDAO;
import ru.pais.vkr.models.Document;
import ru.pais.vkr.models.Storage;
import ru.pais.vkr.utils.StringUtil;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

@Service
public class DocumentService {

    @Autowired
    ModelDAO modelDAO;

    @Autowired
    DocumentDAO documentDAO;

    @Autowired
    StorageDAO storageDAO;

    public Document saveFile(MultipartFile file) throws IOException {
        //Get available storage
        Storage storage = storageDAO.getAvailable();
        if (storage != null) {
            Document document = new Document();
            document.setStorage(storage);
            document.setName(file.getOriginalFilename());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
            String systemName = StringUtil.replaceLast(file.getOriginalFilename(), ".bpmn", "");
            systemName = systemName + " " + simpleDateFormat.format(new Date()) + ".bpmn";
            document.setSystemName(systemName);
            document.setUploadDate(new Date());
            file.transferTo(new File(document.getStorage().getPath() + document.getSystemName()));
            documentDAO.saveDocument(document);
            return document;
        } else {
            //logging
            throw new IOException("");
        }
    }
}
