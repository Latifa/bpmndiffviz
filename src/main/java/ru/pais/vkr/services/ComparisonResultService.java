package ru.pais.vkr.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.dao.ModelDAO;
import ru.pais.vkr.dao.ResultDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

@Service
public class ComparisonResultService {

    @Autowired
    ResultDAO resultDAO;

    public void saveResult(final ComparisonResult comparisonResult) {
        resultDAO.saveResult(comparisonResult);
    }

    public ComparisonResult getResultByID(Long id) {
        return resultDAO.getResultByID(id);
    }

    public List<ComparisonResult> getAllResults() {
        return resultDAO.getAllResults();
    }

}
