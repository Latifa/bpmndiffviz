package ru.pais.vkr.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.comparator.entities.MatchingPair;
import ru.pais.vkr.dao.MatchingPairDAO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

@Service
public class MatchingPairService {

    @Autowired
    MatchingPairDAO matchingPairDAO;

    public void saveMatchingPairs(ComparisonResult comparisonResult) {
        for (MatchingPair matchingPair : comparisonResult.getMatchingSet()) {
            saveMatchingPairMatch(matchingPair, comparisonResult.getId());
        }
        for (MatchingPair matchingPair : comparisonResult.getAddedSet()) {
            saveMatchingPairInsert(matchingPair, comparisonResult.getId());
        }
        for (MatchingPair matchingPair : comparisonResult.getDeletedSet()) {
            saveMatchingPairDelete(matchingPair, comparisonResult.getId());
        }
    }

    public void updateMatchingPairsMatch(ComparisonResult comparisonResult) {
        comparisonResult.setMatchingSet(matchingPairDAO.getMatchingPairsMatchByID(comparisonResult.getId()));
    }


    public void updateMatchingPairsDelete(ComparisonResult comparisonResult) {
        comparisonResult.setDeletedSet(matchingPairDAO.getMatchingPairsDeleteByID(comparisonResult.getId()));
    }

    public void updateMatchingPairsInsert(ComparisonResult comparisonResult) {
        comparisonResult.setAddedSet(matchingPairDAO.getMatchingPairsInsertByID(comparisonResult.getId()));
    }

    public void saveMatchingPairMatch(MatchingPair matchingPair, Long result_id) {
        matchingPairDAO.saveMatchingPairMatch(matchingPair, result_id);
    }

    public void saveMatchingPairInsert(MatchingPair matchingPair, Long result_id) {
        matchingPairDAO.saveMatchingPairInsert(matchingPair, result_id);
    }

    public void saveMatchingPairDelete(MatchingPair matchingPair, Long result_id) {
        matchingPairDAO.saveMatchingPairDelete(matchingPair, result_id);
    }

}
