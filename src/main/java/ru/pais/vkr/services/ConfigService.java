package ru.pais.vkr.services;

import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.*;
import org.camunda.bpm.model.bpmn.instance.Error;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import ru.pais.vkr.comparator.config.BPMNComparator;
import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.comparator.entities.CostConfig;
import ru.pais.vkr.comparator.entities.MatchingPair;
import ru.pais.vkr.comparator.entities.PairType;
import ru.pais.vkr.dao.ConfigDAO;

import java.io.File;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

@Service
public class ConfigService {
    @Autowired
    ConfigDAO configDAO;

    public void saveConfigs(ComparisonResult result) throws IllegalAccessException {
        configDAO.saveConfigs(result.getCostConfig().getAllConfigs(), result.getId());
    }

    public void updateConfigs(ComparisonResult comparisonResult) throws IllegalAccessException {
        comparisonResult.setCostConfig(new CostConfig());
        configDAO.updateConfigs(comparisonResult);
    }

}