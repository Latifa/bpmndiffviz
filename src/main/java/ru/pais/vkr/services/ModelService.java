package ru.pais.vkr.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.pais.vkr.dao.DocumentDAO;
import ru.pais.vkr.dao.ModelDAO;
import ru.pais.vkr.dao.StorageDAO;
import ru.pais.vkr.models.Document;
import ru.pais.vkr.models.Model;
import ru.pais.vkr.models.Storage;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

@Service
public class ModelService {

    @Autowired
    ModelDAO modelDAO;

    @Autowired
    DocumentDAO documentDAO;

    @Autowired
    StorageDAO storageDAO;

    public void saveModel(Model model) {
        modelDAO.saveModel(model);
    }

    public void saveOrUpdateModel(Model model) {
        modelDAO.saveOrUpdateModel(model);
    }

    public void updateModel(Model model) {
        modelDAO.updateModel(model);
    }

    public List<Model> getAllModels() {
        return modelDAO.getAllModels();
    }

    public Model getModelByID(Long id) {
        return modelDAO.getModelByID(id);
    }
}
