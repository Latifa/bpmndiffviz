package ru.pais.vkr.controllers;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ru.pais.vkr.models.Document;
import ru.pais.vkr.models.Model;
import ru.pais.vkr.services.DocumentService;
import ru.pais.vkr.services.ModelService;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
@Controller
@RequestMapping(value = "/resources/")
public class ResourcesRESTController {

    @Autowired
    ModelService modelService;

    @Autowired
    DocumentService documentService;

    @RequestMapping(value = "model", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void saveModel(HttpServletResponse response,
                          @RequestParam(value = "id") Long id) throws IOException {
        Model model = modelService.getModelByID(id);
        File file = new File(model.getDocument().getStorage().getPath() + model.getDocument().getSystemName());
        InputStream is = new FileInputStream(file);
        IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();
    }

}
