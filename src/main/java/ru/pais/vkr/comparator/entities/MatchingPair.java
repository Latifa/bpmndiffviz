package ru.pais.vkr.comparator.entities;

import org.camunda.bpm.model.xml.instance.ModelElementInstance;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class MatchingPair {

    private ModelElementInstance first;
    private ModelElementInstance second;

    private String firstID;
    private String secondID;
    private String definition;
    private PairType type;

    private double differentContainer;
    private double differentLabel;

    private boolean linkSave = true;

    public MatchingPair() {
        differentContainer = 0;
        differentLabel = 0;
    }

    public MatchingPair(ModelElementInstance first, ModelElementInstance second, PairType type) {
        this.first = first;
        this.second = second;
        differentContainer = 0;
        differentLabel = 0;
        this.type = type;
        updateDefinition();
    }

    public MatchingPair(ModelElementInstance first, ModelElementInstance second) {
        this.first = first;
        this.second = second;
        differentContainer = 0;
        differentLabel = 0;
    }

    public MatchingPair(ModelElementInstance first, ModelElementInstance second, int differentContainer, int differentLabel, PairType type) {
        this.first = first;
        this.second = second;
        this.differentContainer = differentContainer;
        this.differentLabel = differentLabel;
        this.type = type;
        updateDefinition();
    }

    public void updateDefinition() {
        if (this.type == PairType.DELETE) {
            definition = "Delete " + first.getElementType().getTypeName() +
                    " with " + (first.getAttributeValue("name") != null ? "name \"".concat(first.getAttributeValue("name")).concat("\"") : "no name");
        }
        if (this.type == PairType.INSERT) {
            definition = "Add " + first.getElementType().getTypeName() +
                    " with " + (first.getAttributeValue("name") != null ? "name \"".concat(first.getAttributeValue("name")).concat("\"") : "no name");
        }
        if (this.type == PairType.MATCH) {
            definition = "Match " + first.getElementType().getTypeName() +
                    " with " + (first.getAttributeValue("name") != null ? "name \"".concat(first.getAttributeValue("name")).concat("\"") : "no name") +
                    " with " + second.getElementType().getTypeName() + " with " +
                    (second.getAttributeValue("name") != null ? "name \"".concat(second.getAttributeValue("name")).concat("\"") : "no name");
        }
    }

    public boolean isLinkSave() {
        return linkSave;
    }

    public void setLinkSave(boolean linkSave) {
        this.linkSave = linkSave;
    }

    public ModelElementInstance getFirst() {
        return first;
    }

    public void setFirst(ModelElementInstance first) {
        this.first = first;
    }

    public ModelElementInstance getSecond() {
        return second;
    }

    public void setSecond(ModelElementInstance second) {
        this.second = second;
    }

    public String getFirstID() {
        return firstID;
    }

    public void setFirstID(String firstID) {
        this.firstID = firstID;
    }

    public String getSecondID() {
        return secondID;
    }

    public void setSecondID(String secondID) {
        this.secondID = secondID;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public PairType getType() {
        return type;
    }

    public void setType(PairType type) {
        this.type = type;
        updateDefinition();
    }

    public double getDifferentContainer() {
        return differentContainer;
    }

    public void setDifferentContainer(double differentContainer) {
        this.differentContainer = differentContainer;
    }

    public double getDifferentLabel() {
        return differentLabel;
    }

    public void setDifferentLabel(double differentLabel) {
        this.differentLabel = differentLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MatchingPair)) return false;

        MatchingPair that = (MatchingPair) o;

        if (Double.compare(that.differentContainer, differentContainer) != 0) return false;
        if (Double.compare(that.differentLabel, differentLabel) != 0) return false;
        if (linkSave != that.linkSave) return false;
        if (first != null ? !first.equals(that.first) : that.first != null) return false;
        if (second != null ? !second.equals(that.second) : that.second != null) return false;
        if (firstID != null ? !firstID.equals(that.firstID) : that.firstID != null) return false;
        if (secondID != null ? !secondID.equals(that.secondID) : that.secondID != null) return false;
        if (definition != null ? !definition.equals(that.definition) : that.definition != null) return false;
        return type == that.type;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = first != null ? first.hashCode() : 0;
        result = 31 * result + (second != null ? second.hashCode() : 0);
        result = 31 * result + (firstID != null ? firstID.hashCode() : 0);
        result = 31 * result + (secondID != null ? secondID.hashCode() : 0);
        result = 31 * result + (definition != null ? definition.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        temp = Double.doubleToLongBits(differentContainer);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(differentLabel);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (linkSave ? 1 : 0);
        return result;
    }
}
