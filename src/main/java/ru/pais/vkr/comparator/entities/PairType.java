package ru.pais.vkr.comparator.entities;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public enum PairType {
    DELETE, INSERT, MATCH
}
