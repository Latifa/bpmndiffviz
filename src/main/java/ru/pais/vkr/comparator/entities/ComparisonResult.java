package ru.pais.vkr.comparator.entities;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.Operation;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import ru.pais.vkr.services.CompareService;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class ComparisonResult implements Comparable<ComparisonResult> {

    private Long id;
    private Date date;

    private BpmnModelInstance bpmnModelInstance1;
    private BpmnModelInstance bpmnModelInstance2;

    private Long firstID;
    private Long secondID;

    private String firstName;
    private String secondName;

    private CostConfig costConfig;
    private int recCounter;
    private double penalty;
    private int heuristic;

    private ArrayList<Class> comparingClassList = new ArrayList<Class>();
    private ArrayList<Class> addedClassList = new ArrayList<Class>();

    Set<MatchingPair> matchingSet;
    Set<MatchingPair> addedSet;
    Set<MatchingPair> deletedSet;

    private double factorLevenshtein = 1;

    public ComparisonResult() {
    }

    public ComparisonResult(BpmnModelInstance bpmnModelInstance1, BpmnModelInstance bpmnModelInstance2, CostConfig costConfig) {
        matchingSet = new HashSet<MatchingPair>();
        addedSet = new HashSet<MatchingPair>();
        deletedSet = new HashSet<MatchingPair>();
        penalty = 0;
        recCounter = 0;
        heuristic = 0;
        this.costConfig = costConfig;
        this.bpmnModelInstance1 = bpmnModelInstance1;
        this.bpmnModelInstance2 = bpmnModelInstance2;
        this.factorLevenshtein = costConfig.getFactorLevenshtein();
    }

    public ComparisonResult(ComparisonResult comparisonResultOld, CostConfig costConfig) {
        matchingSet = new HashSet<MatchingPair>(comparisonResultOld.getMatchingSet());
        addedSet = new HashSet<MatchingPair>(comparisonResultOld.getAddedSet());
        deletedSet = new HashSet<MatchingPair>(comparisonResultOld.getDeletedSet());
        comparingClassList = new ArrayList<Class>(comparisonResultOld.getComparingClassList());
        addedClassList = new ArrayList<Class>(comparisonResultOld.getAddedClassList());
        penalty = comparisonResultOld.getPenalty();
        recCounter = comparisonResultOld.getRecCounter();
        this.costConfig = costConfig;
        heuristic = 0;
        this.bpmnModelInstance1 = comparisonResultOld.getBpmnModelInstance1();
        this.bpmnModelInstance2 = comparisonResultOld.getBpmnModelInstance2();
        this.factorLevenshtein = costConfig.getFactorLevenshtein();
    }

    public ComparisonResult(BpmnModelInstance bpmnModelInstance1, BpmnModelInstance bpmnModelInstance2, List<Class> classMatching, List<Class> classAll, CostConfig costConfig) {
        matchingSet = new HashSet<MatchingPair>();
        addedSet = new HashSet<MatchingPair>();
        deletedSet = new HashSet<MatchingPair>();
        penalty = 0;
        comparingClassList.addAll(classMatching);
        addedClassList.addAll(classAll);
        this.costConfig = costConfig;
        this.bpmnModelInstance1 = bpmnModelInstance1;
        this.bpmnModelInstance2 = bpmnModelInstance2;
        this.factorLevenshtein = costConfig.getFactorLevenshtein();
    }

    public double addPenalty(double nPenalty) {
        penalty += nPenalty;
        return penalty;
    }

    public void addToMatchingSet(MatchingPair matchingPair) {
        matchingSet.add(matchingPair);
    }

    public void addToDeletedSet(MatchingPair matchingPair) {
        deletedSet.add(matchingPair);
    }

    public void addToAddedSet(MatchingPair matchingPair) {
        addedSet.add(matchingPair);
    }

    public void removeFromComparingClassList(Class aClass) {
        comparingClassList.remove(aClass);
    }

    public void removeFromAddedClassList(Class aClass) {
        addedClassList.remove(aClass);
    }

    public int compareTo(ComparisonResult comparisonResult) {
        Double current = this.penalty + this.heuristic;
        Double next = comparisonResult.penalty + comparisonResult.getHeuristic();
        return current.compareTo(next);
    }

    public int getCountByTypeFromMatchingSet(Class aClass) {
        int result = 0;
        for (MatchingPair matchingPair : matchingSet) {
            if (matchingPair.getFirst().getElementType().getTypeName().toLowerCase().equals(aClass.getSimpleName().toLowerCase())) {
                result++;
            }
        }
        return result;
    }

    public int getCountByTypeFromAddedSet(Class aClass) {
        int result = 0;
        for (ModelElementInstance modelElementInstance : getAllKeysFromAddedSet()) {
            if (modelElementInstance.getElementType().getTypeName().toLowerCase().equals(aClass.getSimpleName().toLowerCase())) {
                result++;
            }
        }
        return result;
    }

    public int getCountByTypeFromDeletedSet(Class aClass) {
        int result = 0;
        for (ModelElementInstance modelElementInstance : getAllKeysFromDeletedSet()) {
            if (modelElementInstance.getElementType().getTypeName().toLowerCase().equals(aClass.getSimpleName().toLowerCase())) {
                result++;
            }
        }
        return result;
    }

    public void updateHeuristic() {
        heuristic = 0;
        for (Class aClass : addedClassList) {
            int size1 = CompareService.getModelElementsByType(bpmnModelInstance1, aClass).size();
            int size2 = CompareService.getModelElementsByType(bpmnModelInstance2, aClass).size();
            int ready = getCountByTypeFromDeletedSet(aClass) + getCountByTypeFromMatchingSet(aClass) + getCountByTypeFromAddedSet(aClass);
            size1 -= ready;
            size2 -= ready;
            double resideAdd = 0;
            double resideDelete = 0;
            if (size1 > size2) {
                resideDelete = size1 - size2;
                resideDelete *= costConfig.getDeleteCostByType(aClass);
            }
            if (size2 > size1) {
                resideAdd = size2 - size1;
                resideAdd *= costConfig.getAddCostByType(aClass);
            }
            heuristic += resideAdd + resideDelete;
        }
    }


    public Set<ModelElementInstance> getAllKeysFromDeletedSet() {
        Set<ModelElementInstance> modelElementInstances = new HashSet<ModelElementInstance>();
        deletedSet.stream().forEach(x -> modelElementInstances.add(x.getFirst()));
        return modelElementInstances;
    }


    public Set<ModelElementInstance> getAllKeysFromAddedSet() {
        Set<ModelElementInstance> modelElementInstances = new HashSet<ModelElementInstance>();
        addedSet.stream().forEach(x -> modelElementInstances.add(x.getFirst()));
        return modelElementInstances;
    }

    public Set<ModelElementInstance> getAllKeysFromMatchingSet() {
        Set<ModelElementInstance> modelElementInstances = new HashSet<ModelElementInstance>();
        matchingSet.stream().forEach(x -> modelElementInstances.add(x.getFirst()));
        return modelElementInstances;
    }

    public Set<ModelElementInstance> getAllValuesFromMatchingSet() {
        Set<ModelElementInstance> modelElementInstances = new HashSet<ModelElementInstance>();
        matchingSet.stream().forEach(x -> modelElementInstances.add(x.getSecond()));
        return modelElementInstances;
    }

    public ModelElementInstance getValueByKeyFromMatchingMap(ModelElementInstance modelElementInstanceKey) {
        for (MatchingPair matchingPair : matchingSet) {
            if (matchingPair.getFirst().equals(modelElementInstanceKey)) {
                return matchingPair.getSecond();
            }
        }
        return null;
    }

    public ArrayList<Class> getAddedClassList() {
        return addedClassList;
    }

    public void setAddedClassList(ArrayList<Class> addedClassList) {
        this.addedClassList = addedClassList;
    }

    public ArrayList<Class> getComparingClassList() {
        return comparingClassList;
    }

    public void setComparingClassList(ArrayList<Class> comparingClassList) {
        this.comparingClassList = comparingClassList;
    }

    public CostConfig getCostConfig() {
        return costConfig;
    }

    public void setCostConfig(CostConfig costConfig) {
        this.costConfig = costConfig;
    }

    public Set<MatchingPair> getMatchingSet() {
        return matchingSet;
    }

    public Set<MatchingPair> getAddedSet() {
        return addedSet;
    }

    public void setAddedSet(Set<MatchingPair> addedSet) {
        this.addedSet = addedSet;
    }

    public Set<MatchingPair> getDeletedSet() {
        return deletedSet;
    }

    public void setDeletedSet(Set<MatchingPair> deletedSet) {
        this.deletedSet = deletedSet;
    }

    public void setMatchingSet(Set<MatchingPair> matchingSet) {
        this.matchingSet = matchingSet;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public int getRecCounter() {
        return recCounter;
    }

    public void setRecCounter(int recCounter) {
        this.recCounter = recCounter;
    }

    public int getHeuristic() {
        return heuristic;
    }

    public void setHeuristic(int heuristic) {
        this.heuristic = heuristic;
    }

    public BpmnModelInstance getBpmnModelInstance1() {
        return bpmnModelInstance1;
    }

    public void setBpmnModelInstance1(BpmnModelInstance bpmnModelInstance1) {
        this.bpmnModelInstance1 = bpmnModelInstance1;
    }

    public BpmnModelInstance getBpmnModelInstance2() {
        return bpmnModelInstance2;
    }

    public void setBpmnModelInstance2(BpmnModelInstance bpmnModelInstance2) {
        this.bpmnModelInstance2 = bpmnModelInstance2;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFirstID() {
        return firstID;
    }

    public void setFirstID(Long firstID) {
        this.firstID = firstID;
    }

    public Long getSecondID() {
        return secondID;
    }

    public void setSecondID(Long secondID) {
        this.secondID = secondID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public double getPenalty() {
        return penalty;
    }

    public void setPenalty(double penalty) {
        this.penalty = penalty;
    }

    public void updateMatching(MatchingPair matchingPair) {
        this.addPenalty(matchingPair.getDifferentContainer());
        this.addPenalty(matchingPair.getDifferentLabel());
        this.addToMatchingSet(matchingPair);
        this.updateHeuristic();
    }

    public void updateDeleting(MatchingPair matchingPair) {
        this.addPenalty(matchingPair.getDifferentLabel());
        this.addToDeletedSet(matchingPair);
        this.updateHeuristic();
    }

    public void updateAdding(MatchingPair matchingPair) {
        this.addPenalty(matchingPair.getDifferentLabel());
        this.addToAddedSet(matchingPair);
        this.updateHeuristic();
    }

    public double getFactorLevenshtein() {
        return factorLevenshtein;
    }

    public void setFactorLevenshtein(double factorLevenshtein) {
        this.factorLevenshtein = factorLevenshtein;
    }

    public boolean containsInAnySet(ModelElementInstance modelElementInstance) {
        return this.getAllKeysFromMatchingSet().contains(modelElementInstance) ||
                this.getAllKeysFromDeletedSet().contains(modelElementInstance) ||
                this.getAllKeysFromAddedSet().contains(modelElementInstance);
    }

    public boolean containsInKeysDeletedOrKeysAddedOrValuesMatchingSets(ModelElementInstance modelElementInstance) {
        return this.getAllValuesFromMatchingSet().contains(modelElementInstance) ||
                this.getAllKeysFromDeletedSet().contains(modelElementInstance) ||
                this.getAllKeysFromAddedSet().contains(modelElementInstance);
    }

    public boolean containsInKeysDeletedOrKeysAddedOrKeysMatchingSets(ModelElementInstance modelElementInstance) {
        return this.getAllKeysFromMatchingSet().contains(modelElementInstance) ||
                this.getAllKeysFromDeletedSet().contains(modelElementInstance) ||
                this.getAllKeysFromAddedSet().contains(modelElementInstance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ComparisonResult)) return false;

        ComparisonResult that = (ComparisonResult) o;

        if (recCounter != that.recCounter) return false;
        if (Double.compare(that.penalty, penalty) != 0) return false;
        if (heuristic != that.heuristic) return false;
        if (Double.compare(that.factorLevenshtein, factorLevenshtein) != 0) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (bpmnModelInstance1 != null ? !bpmnModelInstance1.equals(that.bpmnModelInstance1) : that.bpmnModelInstance1 != null)
            return false;
        if (bpmnModelInstance2 != null ? !bpmnModelInstance2.equals(that.bpmnModelInstance2) : that.bpmnModelInstance2 != null)
            return false;
        if (firstID != null ? !firstID.equals(that.firstID) : that.firstID != null) return false;
        if (secondID != null ? !secondID.equals(that.secondID) : that.secondID != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (secondName != null ? !secondName.equals(that.secondName) : that.secondName != null) return false;
        if (costConfig != null ? !costConfig.equals(that.costConfig) : that.costConfig != null) return false;
        if (comparingClassList != null ? !comparingClassList.equals(that.comparingClassList) : that.comparingClassList != null)
            return false;
        if (addedClassList != null ? !addedClassList.equals(that.addedClassList) : that.addedClassList != null)
            return false;
        if (matchingSet != null ? !matchingSet.equals(that.matchingSet) : that.matchingSet != null) return false;
        if (addedSet != null ? !addedSet.equals(that.addedSet) : that.addedSet != null) return false;
        return deletedSet != null ? deletedSet.equals(that.deletedSet) : that.deletedSet == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id != null ? id.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (bpmnModelInstance1 != null ? bpmnModelInstance1.hashCode() : 0);
        result = 31 * result + (bpmnModelInstance2 != null ? bpmnModelInstance2.hashCode() : 0);
        result = 31 * result + (firstID != null ? firstID.hashCode() : 0);
        result = 31 * result + (secondID != null ? secondID.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (costConfig != null ? costConfig.hashCode() : 0);
        result = 31 * result + recCounter;
        temp = Double.doubleToLongBits(penalty);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + heuristic;
        result = 31 * result + (comparingClassList != null ? comparingClassList.hashCode() : 0);
        result = 31 * result + (addedClassList != null ? addedClassList.hashCode() : 0);
        result = 31 * result + (matchingSet != null ? matchingSet.hashCode() : 0);
        result = 31 * result + (addedSet != null ? addedSet.hashCode() : 0);
        result = 31 * result + (deletedSet != null ? deletedSet.hashCode() : 0);
        temp = Double.doubleToLongBits(factorLevenshtein);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
