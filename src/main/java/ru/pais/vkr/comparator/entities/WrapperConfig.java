package ru.pais.vkr.comparator.entities;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import ru.pais.vkr.comparator.config.BPMNComparator;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class WrapperConfig implements BPMNComparator {

    private double insert = 13;
    private double delete = 10;
    private double diffprocess = 5;

    @Override
    public MatchingPair compare(ComparisonResult comparisonResult, BpmnModelInstance bpmnModelInstance1, BpmnModelInstance bpmnModelInstance2, ModelElementInstance modelElementInstance1, ModelElementInstance modelElementInstance2) {
        return null;
    }

    @Override
    public double getDelete() {
        return delete;
    }

    @Override
    public void setDelete(double delete) {
        this.delete = delete;
    }

    @Override
    public double getDiffprocess() {
        return diffprocess;
    }

    @Override
    public void setDiffprocess(double diffprocess) {
        this.diffprocess = diffprocess;
    }

    @Override
    public double getInsert() {
        return insert;
    }

    @Override
    public void setInsert(double insert) {
        this.insert = insert;
    }
}
