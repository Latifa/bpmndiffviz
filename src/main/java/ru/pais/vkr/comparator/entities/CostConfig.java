package ru.pais.vkr.comparator.entities;

import org.camunda.bpm.model.xml.type.ModelElementType;
import ru.pais.vkr.comparator.config.*;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class CostConfig {

    private int unknownDeleteCost = 10;
    private int unknownAddCost = 10;
    private double factorLevenshtein = 1;

    private DefinitionsConfig definitionsConfig;
    private ArtifactConfig artifactConfig;
    private ActivityConfig activityConfig;
    private ErrorEventDefinitionConfig errorEventDefinitionConfig;
    private EscalationEventDefinitionConfig escalationEventDefinitionConfig;
    private DocumentationConfig documentationConfig;
    private OperationConfig operationConfig;
    private ResourceAssignmentExpressionConfig resourceAssignmentExpressionConfig;
    private CorrelationSubscriptionConfig correlationSubscriptionConfig;
    private LaneSetConfig laneSetConfig;
    private ConversationAssociationConfig conversationAssociationConfig;
    private PropertyConfig propertyConfig;
    private CategoryValueConfig categoryValueConfig;
    private CallConversationConfig callConversationConfig;
    private SendTaskConfig sendTaskConfig;
    private ResourceConfig resourceConfig;
    private ConversationConfig conversationConfig;
    private TextConfig textConfig;
    private EndEventConfig endEventConfig;
    private ServiceTaskConfig serviceTaskConfig;
    private CompensateEventDefinitionConfig compensateEventDefinitionConfig;
    private DataInputAssociationConfig dataInputAssociationConfig;
    private DataOutputAssociationConfig dataOutputAssociationConfig;
    private CancelEventDefinitionConfig cancelEventDefinitionConfig;
    private ResourceParameterBindingConfig resourceParameterBindingConfig;
    private DataObjectReferenceConfig dataObjectReferenceConfig;
    private TaskConfig taskConfig;
    private UserTaskConfig userTaskConfig;
    private ProcessConfig processConfig;
    private ExtensionElementsConfig extensionElementsConfig;
    private CorrelationPropertyBindingConfig correlationPropertyBindingConfig;
    private ResourceParameterConfig resourceParameterConfig;
    private ParticipantConfig participantConfig;
    private ComplexBehaviorDefinitionConfig complexBehaviorDefinitionConfig;
    private AssignmentConfig assignmentConfig;
    private PotentialOwnerConfig potentialOwnerConfig;
    private DataObjectConfig dataObjectConfig;
    private ReceiveTaskConfig receiveTaskConfig;
    private CorrelationPropertyRetrievalExpressionConfig correlationPropertyRetrievalExpressionConfig;
    private OutputSetConfig outputSetConfig;
    private ItemDefinitionConfig itemDefinitionConfig;
    private MonitoringConfig monitoringConfig;
    private StartEventConfig startEventConfig;
    private MessageEventDefinitionConfig messageEventDefinitionConfig;
    private LaneConfig laneConfig;
    private MultiInstanceLoopCharacteristicsConfig multiInstanceLoopCharacteristicsConfig;
    private EscalationConfig escalationConfig;
    private ParallelGatewayConfig parallelGatewayConfig;
    private ConversationLinkConfig conversationLinkConfig;
    private ConditionalEventDefinitionConfig conditionalEventDefinitionConfig;
    private CorrelationKeyConfig correlationKeyConfig;
    private IntermediateCatchEventConfig intermediateCatchEventConfig;
    private RenderingConfig renderingConfig;
    private MessageConfig messageConfig;
    private AuditingConfig auditingConfig;
    private AssociationConfig associationConfig;
    private ParticipantMultiplicityConfig participantMultiplicityConfig;
    private IoBindingConfig ioBindingConfig;
    private SignalConfig signalConfig;
    private GlobalConversationConfig globalConversationConfig;
    private InclusiveGatewayConfig inclusiveGatewayConfig;
    private ExclusiveGatewayConfig exclusiveGatewayConfig;
    private SubConversationConfig subConversationConfig;
    private ErrorConfig errorConfig;
    private BusinessRuleTaskConfig businessRuleTaskConfig;
    private EventBasedGatewayConfig eventBasedGatewayConfig;
    private SubProcessConfig subProcessConfig;
    private InterfaceConfig interfaceConfig;
    private CorrelationPropertyConfig correlationPropertyConfig;
    private LinkEventDefinitionConfig linkEventDefinitionConfig;
    private ScriptConfig scriptConfig;
    private SignalEventDefinitionConfig signalEventDefinitionConfig;
    private InputSetConfig inputSetConfig;
    private BoundaryEventConfig boundaryEventConfig;
    private CallActivityConfig callActivityConfig;
    private RelationshipConfig relationshipConfig;
    private EndPointConfig endPointConfig;
    private TerminateEventDefinitionConfig terminateEventDefinitionConfig;
    private IntermediateThrowEventConfig intermediateThrowEventConfig;
    private DataStateConfig dataStateConfig;
    private ExtensionConfig extensionConfig;
    private ManualTaskConfig manualTaskConfig;
    private ComplexGatewayConfig complexGatewayConfig;
    private TextAnnotationConfig textAnnotationConfig;
    private TimerEventDefinitionConfig timerEventDefinitionConfig;
    private IoSpecificationConfig ioSpecificationConfig;
    private ParticipantAssociationConfig participantAssociationConfig;
    private ScriptTaskConfig scriptTaskConfig;
    private SequenceFlowConfig sequenceFlowConfig;
    private MessageFlowAssociationConfig messageFlowAssociationConfig;
    private MessageFlowConfig messageFlowConfig;
    private ThrowEventConfig throwEventConfig;
    private ResourceRoleConfig resourceRoleConfig;
    private PerformerConfig performerConfig;
    private HumanPerformerConfig humanPerformerConfig;
    private FormalExpressionConfig formalExpressionConfig;
    private ExpressionConfig expressionConfig;
    private DataOutputConfig dataOutputConfig;
    private DataInputConfig dataInputConfig;
    private CallableElementConfig callableElementConfig;
    private DataAssociationConfig dataAssociationConfig;
    private CollaborationConfig collaborationConfig;
    private BaseConfig baseConfig;

    public CostConfig() {
        definitionsConfig = new DefinitionsConfig();
        collaborationConfig = new CollaborationConfig();
        dataAssociationConfig = new DataAssociationConfig();
        callableElementConfig = new CallableElementConfig();
        dataInputConfig = new DataInputConfig();
        dataOutputConfig = new DataOutputConfig();
        expressionConfig = new ExpressionConfig();
        formalExpressionConfig = new FormalExpressionConfig();
        humanPerformerConfig = new HumanPerformerConfig();
        performerConfig = new PerformerConfig();
        resourceRoleConfig = new ResourceRoleConfig();
        throwEventConfig = new ThrowEventConfig();
        artifactConfig = new ArtifactConfig();
        activityConfig = new ActivityConfig();
        errorEventDefinitionConfig = new ErrorEventDefinitionConfig();
        escalationEventDefinitionConfig = new EscalationEventDefinitionConfig();
        documentationConfig = new DocumentationConfig();
        operationConfig = new OperationConfig();
        resourceAssignmentExpressionConfig = new ResourceAssignmentExpressionConfig();
        correlationSubscriptionConfig = new CorrelationSubscriptionConfig();
        laneSetConfig = new LaneSetConfig();
        conversationAssociationConfig = new ConversationAssociationConfig();
        propertyConfig = new PropertyConfig();
        categoryValueConfig = new CategoryValueConfig();
        callConversationConfig = new CallConversationConfig();
        sendTaskConfig = new SendTaskConfig();
        resourceConfig = new ResourceConfig();
        conversationConfig = new ConversationConfig();
        textConfig = new TextConfig();
        endEventConfig = new EndEventConfig();
        serviceTaskConfig = new ServiceTaskConfig();
        compensateEventDefinitionConfig = new CompensateEventDefinitionConfig();
        dataInputAssociationConfig = new DataInputAssociationConfig();
        dataOutputAssociationConfig = new DataOutputAssociationConfig();
        cancelEventDefinitionConfig = new CancelEventDefinitionConfig();
        resourceParameterBindingConfig = new ResourceParameterBindingConfig();
        dataObjectReferenceConfig = new DataObjectReferenceConfig();
        taskConfig = new TaskConfig();
        userTaskConfig = new UserTaskConfig();
        processConfig = new ProcessConfig();
        extensionElementsConfig = new ExtensionElementsConfig();
        correlationPropertyBindingConfig = new CorrelationPropertyBindingConfig();
        resourceParameterConfig = new ResourceParameterConfig();
        participantConfig = new ParticipantConfig();
        complexBehaviorDefinitionConfig = new ComplexBehaviorDefinitionConfig();
        assignmentConfig = new AssignmentConfig();
        potentialOwnerConfig = new PotentialOwnerConfig();
        dataObjectConfig = new DataObjectConfig();
        receiveTaskConfig = new ReceiveTaskConfig();
        correlationPropertyRetrievalExpressionConfig = new CorrelationPropertyRetrievalExpressionConfig();
        outputSetConfig = new OutputSetConfig();
        itemDefinitionConfig = new ItemDefinitionConfig();
        monitoringConfig = new MonitoringConfig();
        startEventConfig = new StartEventConfig();
        messageEventDefinitionConfig = new MessageEventDefinitionConfig();
        laneConfig = new LaneConfig();
        multiInstanceLoopCharacteristicsConfig = new MultiInstanceLoopCharacteristicsConfig();
        escalationConfig = new EscalationConfig();
        parallelGatewayConfig = new ParallelGatewayConfig();
        conversationLinkConfig = new ConversationLinkConfig();
        conditionalEventDefinitionConfig = new ConditionalEventDefinitionConfig();
        correlationKeyConfig = new CorrelationKeyConfig();
        intermediateCatchEventConfig = new IntermediateCatchEventConfig();
        renderingConfig = new RenderingConfig();
        messageConfig = new MessageConfig();
        auditingConfig = new AuditingConfig();
        associationConfig = new AssociationConfig();
        participantMultiplicityConfig = new ParticipantMultiplicityConfig();
        ioBindingConfig = new IoBindingConfig();
        signalConfig = new SignalConfig();
        globalConversationConfig = new GlobalConversationConfig();
        inclusiveGatewayConfig = new InclusiveGatewayConfig();
        exclusiveGatewayConfig = new ExclusiveGatewayConfig();
        subConversationConfig = new SubConversationConfig();
        errorConfig = new ErrorConfig();
        businessRuleTaskConfig = new BusinessRuleTaskConfig();
        eventBasedGatewayConfig = new EventBasedGatewayConfig();
        subProcessConfig = new SubProcessConfig();
        interfaceConfig = new InterfaceConfig();
        correlationPropertyConfig = new CorrelationPropertyConfig();
        linkEventDefinitionConfig = new LinkEventDefinitionConfig();
        scriptConfig = new ScriptConfig();
        signalEventDefinitionConfig = new SignalEventDefinitionConfig();
        inputSetConfig = new InputSetConfig();
        boundaryEventConfig = new BoundaryEventConfig();
        callActivityConfig = new CallActivityConfig();
        relationshipConfig = new RelationshipConfig();
        endPointConfig = new EndPointConfig();
        terminateEventDefinitionConfig = new TerminateEventDefinitionConfig();
        intermediateThrowEventConfig = new IntermediateThrowEventConfig();
        dataStateConfig = new DataStateConfig();
        extensionConfig = new ExtensionConfig();
        manualTaskConfig = new ManualTaskConfig();
        complexGatewayConfig = new ComplexGatewayConfig();
        textAnnotationConfig = new TextAnnotationConfig();
        timerEventDefinitionConfig = new TimerEventDefinitionConfig();
        ioSpecificationConfig = new IoSpecificationConfig();
        participantAssociationConfig = new ParticipantAssociationConfig();
        scriptTaskConfig = new ScriptTaskConfig();
        sequenceFlowConfig = new SequenceFlowConfig();
        messageFlowAssociationConfig = new MessageFlowAssociationConfig();
        messageFlowConfig = new MessageFlowConfig();
        baseConfig = new BaseConfig();
    }

    public double  getDeleteCostByType(ModelElementType modelElementType) {
        try {
            Field field = this.getClass().getDeclaredField(modelElementType.getTypeName() + "Config");
            field.setAccessible(true);
            if (field.get(this) != null) {
                Object f = field.get(this);
                Field fieldDelete = f.getClass().getDeclaredField("delete");
                fieldDelete.setAccessible(true);
                return (Double) fieldDelete.get(f);
            } else {
                System.out.println("Config for " + modelElementType.getTypeName() + " not found.");
                return unknownDeleteCost;
            }
        } catch (Exception ex) {
            System.out.println("Config for " + modelElementType.getTypeName() + " not found.");
            return unknownDeleteCost;
        }
    }

    public double getDeleteCostByType(Class aClass) {
        String name = aClass.getSimpleName().substring(0, 1).toLowerCase() + aClass.getSimpleName().substring(1);
        try {
            Field field = this.getClass().getDeclaredField(name + "Config");
            field.setAccessible(true);
            if (field.get(this) != null) {
                Object f = field.get(this);
                Field fieldDelete = f.getClass().getDeclaredField("delete");
                fieldDelete.setAccessible(true);
                return (Double) fieldDelete.get(f);
            } else {
                System.out.println("Config for " + aClass.getSimpleName() + " not found.");
                return unknownDeleteCost;
            }
        } catch (Exception ex) {
            System.out.println("Config for " + aClass.getSimpleName() + " not found.");
            return unknownDeleteCost;
        }
    }

    public double getAddCostByType(ModelElementType modelElementType) {
        try {
            Field field = this.getClass().getDeclaredField(modelElementType.getTypeName() + "Config");
            field.setAccessible(true);
            if (field.get(this) != null) {
                Object f = field.get(this);
                Field fieldDelete = f.getClass().getDeclaredField("insert");
                fieldDelete.setAccessible(true);
                return (Double) fieldDelete.get(f);
            } else {
                System.out.println("Config for " + modelElementType.getTypeName() + " not found.");
                return unknownAddCost;
            }
        } catch (Exception ex) {
            System.out.println("Config for " + modelElementType.getTypeName() + " not found.");
            return unknownAddCost;
        }
    }

    public double getAddCostByType(Class aClass) {
        String name = aClass.getSimpleName().substring(0, 1).toLowerCase() + aClass.getSimpleName().substring(1);
        try {
            Field field = this.getClass().getDeclaredField(name + "Config");
            field.setAccessible(true);
            if (field.get(this) != null) {
                Object f = field.get(this);
                Field fieldDelete = f.getClass().getDeclaredField("insert");
                fieldDelete.setAccessible(true);
                return (Double) fieldDelete.get(f);
            } else {
                System.out.println("Config for " + aClass.getSimpleName() + " not found.");
                return unknownAddCost;
            }
        } catch (Exception ex) {
            System.out.println("Config for " + aClass.getSimpleName() + " not found.");
            return unknownAddCost;
        }
    }

    public void setCosts(String config, Double delete, Double insert, Double diff) throws NoSuchFieldException, IllegalAccessException {
        String name = config.substring(0, 1).toLowerCase() + config.substring(1);

        Field field = this.getClass().getDeclaredField(name + "Config");
        field.setAccessible(true);
        BPMNComparator config1 = (BPMNComparator) field.get(this);
        config1.setDelete(delete);
        config1.setInsert(insert);
        config1.setDiffprocess(diff);
    }

    public List<BPMNComparator> getAllConfigs() throws IllegalAccessException {
        List<BPMNComparator> bpmnComparators = new ArrayList<BPMNComparator>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.getName().endsWith("Config") && !field.getName().equals("baseConfig")) {
                field.setAccessible(true);
                bpmnComparators.add((BPMNComparator) field.get(this));
            }
        }
        return bpmnComparators;
    }

    /**
     * Returns comparator for input bpmn type
     *
     * @param aClass
     * @return
     */
    public BPMNComparator getValidComparator(Class aClass) {
        try {
            String firstChar = aClass.getSimpleName().substring(0, 1).toLowerCase();
            String afterFirstChar = aClass.getSimpleName().substring(1);
            Field field = this.getClass().getDeclaredField(firstChar + afterFirstChar + "Config");
            field.setAccessible(true);
            if (field.get(this) != null) {
                return (BPMNComparator) field.get(this);
            } else {
                System.out.println("Config for " + aClass.getSimpleName() + " not found.");
                return baseConfig;
            }
        } catch (Exception ex) {
            System.out.println("Config for " + aClass.getSimpleName() + " not found.");
            return baseConfig;
        }
    }

    public int getUnknownAddCost() {
        return unknownAddCost;
    }

    public void setUnknownAddCost(int unknownAddCost) {
        this.unknownAddCost = unknownAddCost;
    }

    public int getUnknownDeleteCost() {
        return unknownDeleteCost;
    }

    public void setUnknownDeleteCost(int unknownDeleteCost) {
        this.unknownDeleteCost = unknownDeleteCost;
    }

    public ActivityConfig getActivityConfig() {
        return activityConfig;
    }

    public void setActivityConfig(ActivityConfig activityConfig) {
        this.activityConfig = activityConfig;
    }

    public ArtifactConfig getArtifactConfig() {
        return artifactConfig;
    }

    public void setArtifactConfig(ArtifactConfig artifactConfig) {
        this.artifactConfig = artifactConfig;
    }

    public AssignmentConfig getAssignmentConfig() {
        return assignmentConfig;
    }

    public void setAssignmentConfig(AssignmentConfig assignmentConfig) {
        this.assignmentConfig = assignmentConfig;
    }

    public AssociationConfig getAssociationConfig() {
        return associationConfig;
    }

    public void setAssociationConfig(AssociationConfig associationConfig) {
        this.associationConfig = associationConfig;
    }

    public AuditingConfig getAuditingConfig() {
        return auditingConfig;
    }

    public void setAuditingConfig(AuditingConfig auditingConfig) {
        this.auditingConfig = auditingConfig;
    }

    public BoundaryEventConfig getBoundaryEventConfig() {
        return boundaryEventConfig;
    }

    public void setBoundaryEventConfig(BoundaryEventConfig boundaryEventConfig) {
        this.boundaryEventConfig = boundaryEventConfig;
    }

    public BusinessRuleTaskConfig getBusinessRuleTaskConfig() {
        return businessRuleTaskConfig;
    }

    public void setBusinessRuleTaskConfig(BusinessRuleTaskConfig businessRuleTaskConfig) {
        this.businessRuleTaskConfig = businessRuleTaskConfig;
    }

    public CallableElementConfig getCallableElementConfig() {
        return callableElementConfig;
    }

    public void setCallableElementConfig(CallableElementConfig callableElementConfig) {
        this.callableElementConfig = callableElementConfig;
    }

    public CallActivityConfig getCallActivityConfig() {
        return callActivityConfig;
    }

    public void setCallActivityConfig(CallActivityConfig callActivityConfig) {
        this.callActivityConfig = callActivityConfig;
    }

    public CallConversationConfig getCallConversationConfig() {
        return callConversationConfig;
    }

    public void setCallConversationConfig(CallConversationConfig callConversationConfig) {
        this.callConversationConfig = callConversationConfig;
    }

    public CancelEventDefinitionConfig getCancelEventDefinitionConfig() {
        return cancelEventDefinitionConfig;
    }

    public void setCancelEventDefinitionConfig(CancelEventDefinitionConfig cancelEventDefinitionConfig) {
        this.cancelEventDefinitionConfig = cancelEventDefinitionConfig;
    }

    public CategoryValueConfig getCategoryValueConfig() {
        return categoryValueConfig;
    }

    public void setCategoryValueConfig(CategoryValueConfig categoryValueConfig) {
        this.categoryValueConfig = categoryValueConfig;
    }

    public CollaborationConfig getCollaborationConfig() {
        return collaborationConfig;
    }

    public void setCollaborationConfig(CollaborationConfig collaborationConfig) {
        this.collaborationConfig = collaborationConfig;
    }

    public CompensateEventDefinitionConfig getCompensateEventDefinitionConfig() {
        return compensateEventDefinitionConfig;
    }

    public void setCompensateEventDefinitionConfig(CompensateEventDefinitionConfig compensateEventDefinitionConfig) {
        this.compensateEventDefinitionConfig = compensateEventDefinitionConfig;
    }

    public ComplexBehaviorDefinitionConfig getComplexBehaviorDefinitionConfig() {
        return complexBehaviorDefinitionConfig;
    }

    public void setComplexBehaviorDefinitionConfig(ComplexBehaviorDefinitionConfig complexBehaviorDefinitionConfig) {
        this.complexBehaviorDefinitionConfig = complexBehaviorDefinitionConfig;
    }

    public ComplexGatewayConfig getComplexGatewayConfig() {
        return complexGatewayConfig;
    }

    public void setComplexGatewayConfig(ComplexGatewayConfig complexGatewayConfig) {
        this.complexGatewayConfig = complexGatewayConfig;
    }

    public ConditionalEventDefinitionConfig getConditionalEventDefinitionConfig() {
        return conditionalEventDefinitionConfig;
    }

    public void setConditionalEventDefinitionConfig(ConditionalEventDefinitionConfig conditionalEventDefinitionConfig) {
        this.conditionalEventDefinitionConfig = conditionalEventDefinitionConfig;
    }

    public ConversationAssociationConfig getConversationAssociationConfig() {
        return conversationAssociationConfig;
    }

    public void setConversationAssociationConfig(ConversationAssociationConfig conversationAssociationConfig) {
        this.conversationAssociationConfig = conversationAssociationConfig;
    }

    public ConversationConfig getConversationConfig() {
        return conversationConfig;
    }

    public void setConversationConfig(ConversationConfig conversationConfig) {
        this.conversationConfig = conversationConfig;
    }

    public ConversationLinkConfig getConversationLinkConfig() {
        return conversationLinkConfig;
    }

    public void setConversationLinkConfig(ConversationLinkConfig conversationLinkConfig) {
        this.conversationLinkConfig = conversationLinkConfig;
    }

    public CorrelationKeyConfig getCorrelationKeyConfig() {
        return correlationKeyConfig;
    }

    public void setCorrelationKeyConfig(CorrelationKeyConfig correlationKeyConfig) {
        this.correlationKeyConfig = correlationKeyConfig;
    }

    public CorrelationPropertyBindingConfig getCorrelationPropertyBindingConfig() {
        return correlationPropertyBindingConfig;
    }

    public void setCorrelationPropertyBindingConfig(CorrelationPropertyBindingConfig correlationPropertyBindingConfig) {
        this.correlationPropertyBindingConfig = correlationPropertyBindingConfig;
    }

    public CorrelationPropertyConfig getCorrelationPropertyConfig() {
        return correlationPropertyConfig;
    }

    public void setCorrelationPropertyConfig(CorrelationPropertyConfig correlationPropertyConfig) {
        this.correlationPropertyConfig = correlationPropertyConfig;
    }

    public CorrelationPropertyRetrievalExpressionConfig getCorrelationPropertyRetrievalExpressionConfig() {
        return correlationPropertyRetrievalExpressionConfig;
    }

    public void setCorrelationPropertyRetrievalExpressionConfig(CorrelationPropertyRetrievalExpressionConfig correlationPropertyRetrievalExpressionConfig) {
        this.correlationPropertyRetrievalExpressionConfig = correlationPropertyRetrievalExpressionConfig;
    }

    public CorrelationSubscriptionConfig getCorrelationSubscriptionConfig() {
        return correlationSubscriptionConfig;
    }

    public void setCorrelationSubscriptionConfig(CorrelationSubscriptionConfig correlationSubscriptionConfig) {
        this.correlationSubscriptionConfig = correlationSubscriptionConfig;
    }

    public DataAssociationConfig getDataAssociationConfig() {
        return dataAssociationConfig;
    }

    public void setDataAssociationConfig(DataAssociationConfig dataAssociationConfig) {
        this.dataAssociationConfig = dataAssociationConfig;
    }

    public DataInputAssociationConfig getDataInputAssociationConfig() {
        return dataInputAssociationConfig;
    }

    public void setDataInputAssociationConfig(DataInputAssociationConfig dataInputAssociationConfig) {
        this.dataInputAssociationConfig = dataInputAssociationConfig;
    }

    public DataInputConfig getDataInputConfig() {
        return dataInputConfig;
    }

    public void setDataInputConfig(DataInputConfig dataInputConfig) {
        this.dataInputConfig = dataInputConfig;
    }

    public DataObjectConfig getDataObjectConfig() {
        return dataObjectConfig;
    }

    public void setDataObjectConfig(DataObjectConfig dataObjectConfig) {
        this.dataObjectConfig = dataObjectConfig;
    }

    public DataObjectReferenceConfig getDataObjectReferenceConfig() {
        return dataObjectReferenceConfig;
    }

    public void setDataObjectReferenceConfig(DataObjectReferenceConfig dataObjectReferenceConfig) {
        this.dataObjectReferenceConfig = dataObjectReferenceConfig;
    }

    public DataOutputAssociationConfig getDataOutputAssociationConfig() {
        return dataOutputAssociationConfig;
    }

    public void setDataOutputAssociationConfig(DataOutputAssociationConfig dataOutputAssociationConfig) {
        this.dataOutputAssociationConfig = dataOutputAssociationConfig;
    }

    public DataOutputConfig getDataOutputConfig() {
        return dataOutputConfig;
    }

    public void setDataOutputConfig(DataOutputConfig dataOutputConfig) {
        this.dataOutputConfig = dataOutputConfig;
    }

    public DataStateConfig getDataStateConfig() {
        return dataStateConfig;
    }

    public void setDataStateConfig(DataStateConfig dataStateConfig) {
        this.dataStateConfig = dataStateConfig;
    }

    public DocumentationConfig getDocumentationConfig() {
        return documentationConfig;
    }

    public void setDocumentationConfig(DocumentationConfig documentationConfig) {
        this.documentationConfig = documentationConfig;
    }

    public EndEventConfig getEndEventConfig() {
        return endEventConfig;
    }

    public void setEndEventConfig(EndEventConfig endEventConfig) {
        this.endEventConfig = endEventConfig;
    }

    public EndPointConfig getEndPointConfig() {
        return endPointConfig;
    }

    public void setEndPointConfig(EndPointConfig endPointConfig) {
        this.endPointConfig = endPointConfig;
    }

    public ErrorConfig getErrorConfig() {
        return errorConfig;
    }

    public void setErrorConfig(ErrorConfig errorConfig) {
        this.errorConfig = errorConfig;
    }

    public ErrorEventDefinitionConfig getErrorEventDefinitionConfig() {
        return errorEventDefinitionConfig;
    }

    public void setErrorEventDefinitionConfig(ErrorEventDefinitionConfig errorEventDefinitionConfig) {
        this.errorEventDefinitionConfig = errorEventDefinitionConfig;
    }

    public EscalationConfig getEscalationConfig() {
        return escalationConfig;
    }

    public void setEscalationConfig(EscalationConfig escalationConfig) {
        this.escalationConfig = escalationConfig;
    }

    public EscalationEventDefinitionConfig getEscalationEventDefinitionConfig() {
        return escalationEventDefinitionConfig;
    }

    public void setEscalationEventDefinitionConfig(EscalationEventDefinitionConfig escalationEventDefinitionConfig) {
        this.escalationEventDefinitionConfig = escalationEventDefinitionConfig;
    }

    public EventBasedGatewayConfig getEventBasedGatewayConfig() {
        return eventBasedGatewayConfig;
    }

    public void setEventBasedGatewayConfig(EventBasedGatewayConfig eventBasedGatewayConfig) {
        this.eventBasedGatewayConfig = eventBasedGatewayConfig;
    }

    public ExclusiveGatewayConfig getExclusiveGatewayConfig() {
        return exclusiveGatewayConfig;
    }

    public void setExclusiveGatewayConfig(ExclusiveGatewayConfig exclusiveGatewayConfig) {
        this.exclusiveGatewayConfig = exclusiveGatewayConfig;
    }

    public ExpressionConfig getExpressionConfig() {
        return expressionConfig;
    }

    public void setExpressionConfig(ExpressionConfig expressionConfig) {
        this.expressionConfig = expressionConfig;
    }

    public ExtensionConfig getExtensionConfig() {
        return extensionConfig;
    }

    public void setExtensionConfig(ExtensionConfig extensionConfig) {
        this.extensionConfig = extensionConfig;
    }

    public ExtensionElementsConfig getExtensionElementsConfig() {
        return extensionElementsConfig;
    }

    public void setExtensionElementsConfig(ExtensionElementsConfig extensionElementsConfig) {
        this.extensionElementsConfig = extensionElementsConfig;
    }

    public FormalExpressionConfig getFormalExpressionConfig() {
        return formalExpressionConfig;
    }

    public void setFormalExpressionConfig(FormalExpressionConfig formalExpressionConfig) {
        this.formalExpressionConfig = formalExpressionConfig;
    }

    public GlobalConversationConfig getGlobalConversationConfig() {
        return globalConversationConfig;
    }

    public void setGlobalConversationConfig(GlobalConversationConfig globalConversationConfig) {
        this.globalConversationConfig = globalConversationConfig;
    }

    public HumanPerformerConfig getHumanPerformerConfig() {
        return humanPerformerConfig;
    }

    public void setHumanPerformerConfig(HumanPerformerConfig humanPerformerConfig) {
        this.humanPerformerConfig = humanPerformerConfig;
    }

    public InclusiveGatewayConfig getInclusiveGatewayConfig() {
        return inclusiveGatewayConfig;
    }

    public void setInclusiveGatewayConfig(InclusiveGatewayConfig inclusiveGatewayConfig) {
        this.inclusiveGatewayConfig = inclusiveGatewayConfig;
    }

    public InputSetConfig getInputSetConfig() {
        return inputSetConfig;
    }

    public void setInputSetConfig(InputSetConfig inputSetConfig) {
        this.inputSetConfig = inputSetConfig;
    }

    public InterfaceConfig getInterfaceConfig() {
        return interfaceConfig;
    }

    public void setInterfaceConfig(InterfaceConfig interfaceConfig) {
        this.interfaceConfig = interfaceConfig;
    }

    public IntermediateCatchEventConfig getIntermediateCatchEventConfig() {
        return intermediateCatchEventConfig;
    }

    public void setIntermediateCatchEventConfig(IntermediateCatchEventConfig intermediateCatchEventConfig) {
        this.intermediateCatchEventConfig = intermediateCatchEventConfig;
    }

    public IntermediateThrowEventConfig getIntermediateThrowEventConfig() {
        return intermediateThrowEventConfig;
    }

    public void setIntermediateThrowEventConfig(IntermediateThrowEventConfig intermediateThrowEventConfig) {
        this.intermediateThrowEventConfig = intermediateThrowEventConfig;
    }

    public IoBindingConfig getIoBindingConfig() {
        return ioBindingConfig;
    }

    public void setIoBindingConfig(IoBindingConfig ioBindingConfig) {
        this.ioBindingConfig = ioBindingConfig;
    }

    public IoSpecificationConfig getIoSpecificationConfig() {
        return ioSpecificationConfig;
    }

    public void setIoSpecificationConfig(IoSpecificationConfig ioSpecificationConfig) {
        this.ioSpecificationConfig = ioSpecificationConfig;
    }

    public ItemDefinitionConfig getItemDefinitionConfig() {
        return itemDefinitionConfig;
    }

    public void setItemDefinitionConfig(ItemDefinitionConfig itemDefinitionConfig) {
        this.itemDefinitionConfig = itemDefinitionConfig;
    }

    public LaneConfig getLaneConfig() {
        return laneConfig;
    }

    public void setLaneConfig(LaneConfig laneConfig) {
        this.laneConfig = laneConfig;
    }

    public LaneSetConfig getLaneSetConfig() {
        return laneSetConfig;
    }

    public void setLaneSetConfig(LaneSetConfig laneSetConfig) {
        this.laneSetConfig = laneSetConfig;
    }

    public LinkEventDefinitionConfig getLinkEventDefinitionConfig() {
        return linkEventDefinitionConfig;
    }

    public void setLinkEventDefinitionConfig(LinkEventDefinitionConfig linkEventDefinitionConfig) {
        this.linkEventDefinitionConfig = linkEventDefinitionConfig;
    }

    public ManualTaskConfig getManualTaskConfig() {
        return manualTaskConfig;
    }

    public void setManualTaskConfig(ManualTaskConfig manualTaskConfig) {
        this.manualTaskConfig = manualTaskConfig;
    }

    public MessageConfig getMessageConfig() {
        return messageConfig;
    }

    public void setMessageConfig(MessageConfig messageConfig) {
        this.messageConfig = messageConfig;
    }

    public MessageEventDefinitionConfig getMessageEventDefinitionConfig() {
        return messageEventDefinitionConfig;
    }

    public void setMessageEventDefinitionConfig(MessageEventDefinitionConfig messageEventDefinitionConfig) {
        this.messageEventDefinitionConfig = messageEventDefinitionConfig;
    }

    public MessageFlowAssociationConfig getMessageFlowAssociationConfig() {
        return messageFlowAssociationConfig;
    }

    public void setMessageFlowAssociationConfig(MessageFlowAssociationConfig messageFlowAssociationConfig) {
        this.messageFlowAssociationConfig = messageFlowAssociationConfig;
    }

    public MessageFlowConfig getMessageFlowConfig() {
        return messageFlowConfig;
    }

    public void setMessageFlowConfig(MessageFlowConfig messageFlowConfig) {
        this.messageFlowConfig = messageFlowConfig;
    }

    public MonitoringConfig getMonitoringConfig() {
        return monitoringConfig;
    }

    public void setMonitoringConfig(MonitoringConfig monitoringConfig) {
        this.monitoringConfig = monitoringConfig;
    }

    public MultiInstanceLoopCharacteristicsConfig getMultiInstanceLoopCharacteristicsConfig() {
        return multiInstanceLoopCharacteristicsConfig;
    }

    public void setMultiInstanceLoopCharacteristicsConfig(MultiInstanceLoopCharacteristicsConfig multiInstanceLoopCharacteristicsConfig) {
        this.multiInstanceLoopCharacteristicsConfig = multiInstanceLoopCharacteristicsConfig;
    }

    public OperationConfig getOperationConfig() {
        return operationConfig;
    }

    public void setOperationConfig(OperationConfig operationConfig) {
        this.operationConfig = operationConfig;
    }

    public OutputSetConfig getOutputSetConfig() {
        return outputSetConfig;
    }

    public void setOutputSetConfig(OutputSetConfig outputSetConfig) {
        this.outputSetConfig = outputSetConfig;
    }

    public ParallelGatewayConfig getParallelGatewayConfig() {
        return parallelGatewayConfig;
    }

    public void setParallelGatewayConfig(ParallelGatewayConfig parallelGatewayConfig) {
        this.parallelGatewayConfig = parallelGatewayConfig;
    }

    public ParticipantAssociationConfig getParticipantAssociationConfig() {
        return participantAssociationConfig;
    }

    public void setParticipantAssociationConfig(ParticipantAssociationConfig participantAssociationConfig) {
        this.participantAssociationConfig = participantAssociationConfig;
    }

    public ParticipantConfig getParticipantConfig() {
        return participantConfig;
    }

    public void setParticipantConfig(ParticipantConfig participantConfig) {
        this.participantConfig = participantConfig;
    }

    public ParticipantMultiplicityConfig getParticipantMultiplicityConfig() {
        return participantMultiplicityConfig;
    }

    public void setParticipantMultiplicityConfig(ParticipantMultiplicityConfig participantMultiplicityConfig) {
        this.participantMultiplicityConfig = participantMultiplicityConfig;
    }

    public PerformerConfig getPerformerConfig() {
        return performerConfig;
    }

    public void setPerformerConfig(PerformerConfig performerConfig) {
        this.performerConfig = performerConfig;
    }

    public PotentialOwnerConfig getPotentialOwnerConfig() {
        return potentialOwnerConfig;
    }

    public void setPotentialOwnerConfig(PotentialOwnerConfig potentialOwnerConfig) {
        this.potentialOwnerConfig = potentialOwnerConfig;
    }

    public ProcessConfig getProcessConfig() {
        return processConfig;
    }

    public void setProcessConfig(ProcessConfig processConfig) {
        this.processConfig = processConfig;
    }

    public PropertyConfig getPropertyConfig() {
        return propertyConfig;
    }

    public void setPropertyConfig(PropertyConfig propertyConfig) {
        this.propertyConfig = propertyConfig;
    }

    public ReceiveTaskConfig getReceiveTaskConfig() {
        return receiveTaskConfig;
    }

    public void setReceiveTaskConfig(ReceiveTaskConfig receiveTaskConfig) {
        this.receiveTaskConfig = receiveTaskConfig;
    }

    public RelationshipConfig getRelationshipConfig() {
        return relationshipConfig;
    }

    public void setRelationshipConfig(RelationshipConfig relationshipConfig) {
        this.relationshipConfig = relationshipConfig;
    }

    public RenderingConfig getRenderingConfig() {
        return renderingConfig;
    }

    public void setRenderingConfig(RenderingConfig renderingConfig) {
        this.renderingConfig = renderingConfig;
    }

    public ResourceAssignmentExpressionConfig getResourceAssignmentExpressionConfig() {
        return resourceAssignmentExpressionConfig;
    }

    public void setResourceAssignmentExpressionConfig(ResourceAssignmentExpressionConfig resourceAssignmentExpressionConfig) {
        this.resourceAssignmentExpressionConfig = resourceAssignmentExpressionConfig;
    }

    public ResourceConfig getResourceConfig() {
        return resourceConfig;
    }

    public void setResourceConfig(ResourceConfig resourceConfig) {
        this.resourceConfig = resourceConfig;
    }

    public ResourceParameterBindingConfig getResourceParameterBindingConfig() {
        return resourceParameterBindingConfig;
    }

    public void setResourceParameterBindingConfig(ResourceParameterBindingConfig resourceParameterBindingConfig) {
        this.resourceParameterBindingConfig = resourceParameterBindingConfig;
    }

    public ResourceParameterConfig getResourceParameterConfig() {
        return resourceParameterConfig;
    }

    public void setResourceParameterConfig(ResourceParameterConfig resourceParameterConfig) {
        this.resourceParameterConfig = resourceParameterConfig;
    }

    public ResourceRoleConfig getResourceRoleConfig() {
        return resourceRoleConfig;
    }

    public void setResourceRoleConfig(ResourceRoleConfig resourceRoleConfig) {
        this.resourceRoleConfig = resourceRoleConfig;
    }

    public ScriptConfig getScriptConfig() {
        return scriptConfig;
    }

    public void setScriptConfig(ScriptConfig scriptConfig) {
        this.scriptConfig = scriptConfig;
    }

    public ScriptTaskConfig getScriptTaskConfig() {
        return scriptTaskConfig;
    }

    public void setScriptTaskConfig(ScriptTaskConfig scriptTaskConfig) {
        this.scriptTaskConfig = scriptTaskConfig;
    }

    public SendTaskConfig getSendTaskConfig() {
        return sendTaskConfig;
    }

    public void setSendTaskConfig(SendTaskConfig sendTaskConfig) {
        this.sendTaskConfig = sendTaskConfig;
    }

    public SequenceFlowConfig getSequenceFlowConfig() {
        return sequenceFlowConfig;
    }

    public void setSequenceFlowConfig(SequenceFlowConfig sequenceFlowConfig) {
        this.sequenceFlowConfig = sequenceFlowConfig;
    }

    public ServiceTaskConfig getServiceTaskConfig() {
        return serviceTaskConfig;
    }

    public void setServiceTaskConfig(ServiceTaskConfig serviceTaskConfig) {
        this.serviceTaskConfig = serviceTaskConfig;
    }

    public SignalConfig getSignalConfig() {
        return signalConfig;
    }

    public void setSignalConfig(SignalConfig signalConfig) {
        this.signalConfig = signalConfig;
    }

    public SignalEventDefinitionConfig getSignalEventDefinitionConfig() {
        return signalEventDefinitionConfig;
    }

    public void setSignalEventDefinitionConfig(SignalEventDefinitionConfig signalEventDefinitionConfig) {
        this.signalEventDefinitionConfig = signalEventDefinitionConfig;
    }

    public StartEventConfig getStartEventConfig() {
        return startEventConfig;
    }

    public void setStartEventConfig(StartEventConfig startEventConfig) {
        this.startEventConfig = startEventConfig;
    }

    public SubConversationConfig getSubConversationConfig() {
        return subConversationConfig;
    }

    public void setSubConversationConfig(SubConversationConfig subConversationConfig) {
        this.subConversationConfig = subConversationConfig;
    }

    public SubProcessConfig getSubProcessConfig() {
        return subProcessConfig;
    }

    public void setSubProcessConfig(SubProcessConfig subProcessConfig) {
        this.subProcessConfig = subProcessConfig;
    }

    public TaskConfig getTaskConfig() {
        return taskConfig;
    }

    public void setTaskConfig(TaskConfig taskConfig) {
        this.taskConfig = taskConfig;
    }

    public TerminateEventDefinitionConfig getTerminateEventDefinitionConfig() {
        return terminateEventDefinitionConfig;
    }

    public void setTerminateEventDefinitionConfig(TerminateEventDefinitionConfig terminateEventDefinitionConfig) {
        this.terminateEventDefinitionConfig = terminateEventDefinitionConfig;
    }

    public TextAnnotationConfig getTextAnnotationConfig() {
        return textAnnotationConfig;
    }

    public void setTextAnnotationConfig(TextAnnotationConfig textAnnotationConfig) {
        this.textAnnotationConfig = textAnnotationConfig;
    }

    public TextConfig getTextConfig() {
        return textConfig;
    }

    public void setTextConfig(TextConfig textConfig) {
        this.textConfig = textConfig;
    }

    public ThrowEventConfig getThrowEventConfig() {
        return throwEventConfig;
    }

    public void setThrowEventConfig(ThrowEventConfig throwEventConfig) {
        this.throwEventConfig = throwEventConfig;
    }

    public TimerEventDefinitionConfig getTimerEventDefinitionConfig() {
        return timerEventDefinitionConfig;
    }

    public void setTimerEventDefinitionConfig(TimerEventDefinitionConfig timerEventDefinitionConfig) {
        this.timerEventDefinitionConfig = timerEventDefinitionConfig;
    }

    public UserTaskConfig getUserTaskConfig() {
        return userTaskConfig;
    }

    public void setUserTaskConfig(UserTaskConfig userTaskConfig) {
        this.userTaskConfig = userTaskConfig;
    }

    public BaseConfig getBaseConfig() {
        return baseConfig;
    }

    public void setBaseConfig(BaseConfig baseConfig) {
        this.baseConfig = baseConfig;
    }

    public DefinitionsConfig getDefinitionsConfig() {
        return definitionsConfig;
    }

    public void setDefinitionsConfig(DefinitionsConfig definitionsConfig) {
        this.definitionsConfig = definitionsConfig;
    }

    public double getFactorLevenshtein() {
        return factorLevenshtein;
    }

    public void setFactorLevenshtein(double factorLevenshtein) {
        this.factorLevenshtein = factorLevenshtein;
    }
}
