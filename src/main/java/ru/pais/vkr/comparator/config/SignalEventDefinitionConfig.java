package ru.pais.vkr.comparator.config;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.SignalEventDefinition;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.comparator.entities.MatchingPair;
import ru.pais.vkr.utils.SameForAllConfig;
import ru.pais.vkr.utils.StringUtil;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class SignalEventDefinitionConfig implements BPMNComparator {

    private boolean changed = false;

    private double insert = 13;
    private double delete = 10;
    private double diffprocess = 5;

    @Override
    public MatchingPair compare(ComparisonResult comparisonResult, BpmnModelInstance bpmnModelInstance1, BpmnModelInstance bpmnModelInstance2, ModelElementInstance modelElementInstance1, ModelElementInstance modelElementInstance2) {
        SignalEventDefinition signalEventDefinition1 = (SignalEventDefinition) modelElementInstance1;
        SignalEventDefinition signalEventDefinition2 = (SignalEventDefinition) modelElementInstance2;

        String name1 = signalEventDefinition1.getAttributeValue("name");
        if (name1 == null) {
            name1 = "";
        }
        String name2 = signalEventDefinition2.getAttributeValue("name");
        if (name2 == null) {
            name2 = "";
        }

        double differentLabel = StringUtil.computeLevenshteinDistance(name1, name2);
        differentLabel *= comparisonResult.getFactorLevenshtein();
        double differentContainer = 0;
        if (modelElementInstance1.getParentElement() != null && modelElementInstance2.getParentElement() != null) {
            if (SameForAllConfig.sameContainer(modelElementInstance1, modelElementInstance2, comparisonResult)) {
                differentContainer = diffprocess;
            }
        }
        MatchingPair matchingPair = new MatchingPair(modelElementInstance1, modelElementInstance2);
        matchingPair.setDifferentLabel(differentLabel);
        matchingPair.setDifferentContainer(differentContainer);
        return matchingPair;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public double getDelete() {
        return delete;
    }

    public void setDelete(double delete) {
        this.delete = delete;
        changed = true;
    }

    public double getDiffprocess() {
        return diffprocess;
    }

    public void setDiffprocess(double diffprocess) {
        this.diffprocess = diffprocess;
        changed = true;
    }

    public double getInsert() {
        return insert;
    }

    public void setInsert(double insert) {
        this.insert = insert;
        changed = true;
    }

}

