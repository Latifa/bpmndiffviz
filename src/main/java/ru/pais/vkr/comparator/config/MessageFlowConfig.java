package ru.pais.vkr.comparator.config;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.MessageFlow;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.comparator.entities.MatchingPair;
import ru.pais.vkr.utils.SameForAllConfig;
import ru.pais.vkr.utils.StringUtil;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class MessageFlowConfig implements BPMNComparator {

    private boolean changed = false;

    private double insert = 5;
    private double delete = 2;
    private double diffprocess = 5;

    public MatchingPair compare(ComparisonResult comparisonResult, BpmnModelInstance bpmnModelInstance1, BpmnModelInstance bpmnModelInstance2, ModelElementInstance modelElementInstance1, ModelElementInstance modelElementInstance2) {
        MessageFlow messageFlow1 = (MessageFlow) modelElementInstance1;
        MessageFlow messageFlow2 = (MessageFlow) modelElementInstance2;

        ModelElementInstance modelElementInstance1Source = messageFlow1.getSource();
        ModelElementInstance modelElementInstance2Source = messageFlow2.getSource();
        ModelElementInstance modelElementInstance1Target = messageFlow1.getTarget();
        ModelElementInstance modelElementInstance2Target = messageFlow2.getTarget();

        ModelElementInstance modelElementInstance1SourceFromMap =
                comparisonResult.getValueByKeyFromMatchingMap(modelElementInstance1Source);
        ModelElementInstance modelElementInstance1TargetFromMap =
                comparisonResult.getValueByKeyFromMatchingMap(modelElementInstance1Target);

        //Sources and targets are equals
        if (modelElementInstance1SourceFromMap != null &&
                modelElementInstance1TargetFromMap != null &&
                modelElementInstance1SourceFromMap.equals(modelElementInstance2Source) &&
                modelElementInstance1TargetFromMap.equals(modelElementInstance2Target)) {
            String name1 = modelElementInstance1.getAttributeValue("name");
            if (name1 == null) {
                name1 = "";
            }
            String name2 = modelElementInstance2.getAttributeValue("name");
            if (name2 == null) {
                name2 = "";
            }
            double differentLabel = StringUtil.computeLevenshteinDistance(name1, name2);
            differentLabel *= comparisonResult.getFactorLevenshtein();
            double differentContainer = 0;

            if (modelElementInstance1.getParentElement() != null && modelElementInstance2.getParentElement() != null) {
                if (SameForAllConfig.sameContainer(modelElementInstance1, modelElementInstance2, comparisonResult)) {
                    differentContainer = diffprocess;
                }
            }

            MatchingPair matchingPair = new MatchingPair(modelElementInstance1, modelElementInstance2);
            matchingPair.setDifferentLabel(differentLabel);
            matchingPair.setDifferentContainer(differentContainer);
            return matchingPair;
        } else {
            String name1 = modelElementInstance1.getAttributeValue("name");
            if (name1 == null) {
                name1 = "";
            }
            String name2 = modelElementInstance2.getAttributeValue("name");
            if (name2 == null) {
                name2 = "";
            }
            double differentLabel = StringUtil.computeLevenshteinDistance(name1, name2);
            differentLabel *= comparisonResult.getFactorLevenshtein();
            double differentContainer = 0;

            if (modelElementInstance1.getParentElement() != null && modelElementInstance2.getParentElement() != null) {
                if (SameForAllConfig.sameContainer(modelElementInstance1, modelElementInstance2, comparisonResult)) {
                    differentContainer = diffprocess;
                }
            }

            MatchingPair matchingPair = new MatchingPair(modelElementInstance1, modelElementInstance2);
            matchingPair.setDifferentLabel(differentLabel);
            matchingPair.setDifferentContainer(differentContainer);
            matchingPair.setLinkSave(false);
            return matchingPair;
        }

    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public double getDelete() {
        return delete;
    }

    public void setDelete(double delete) {
        this.delete = delete;
        changed = true;
    }

    public double getDiffprocess() {
        return diffprocess;
    }

    public void setDiffprocess(double diffprocess) {
        this.diffprocess = diffprocess;
        changed = true;
    }

    public double getInsert() {
        return insert;
    }

    public void setInsert(double insert) {
        this.insert = insert;
        changed = true;
    }
}

