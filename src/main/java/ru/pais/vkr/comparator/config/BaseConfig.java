package ru.pais.vkr.comparator.config;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.comparator.entities.MatchingPair;
import ru.pais.vkr.utils.SameForAllConfig;
import ru.pais.vkr.utils.StringUtil;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class BaseConfig implements BPMNComparator {

    public MatchingPair compare(ComparisonResult comparisonResult,
                                BpmnModelInstance bpmnModelInstance1,
                                BpmnModelInstance bpmnModelInstance2,
                                ModelElementInstance modelElementInstance1,
                                ModelElementInstance modelElementInstance2) {
        String name1 = modelElementInstance1.getAttributeValue("name");
        if (name1 == null) {
            name1 = "";
        }
        String name2 = modelElementInstance2.getAttributeValue("name");
        if (name2 == null) {
            name2 = "";
        }

        double differentLabel = StringUtil.computeLevenshteinDistance(name1, name2);
        differentLabel *= comparisonResult.getFactorLevenshtein();
        double differentContainer = 0;

        if (modelElementInstance1.getParentElement() != null && modelElementInstance2.getParentElement() != null) {
            if (SameForAllConfig.sameContainer(modelElementInstance1, modelElementInstance2, comparisonResult)) {
                differentContainer = 5;
            }
        }

        MatchingPair matchingPair = new MatchingPair(modelElementInstance1, modelElementInstance2);
        matchingPair.setDifferentLabel(differentLabel);
        matchingPair.setDifferentContainer(differentContainer);
        return matchingPair;
    }

    public double getDelete() {
        return 0;
    }

    public void setDelete(double delete) {

    }

    public double getDiffprocess() {
        return 0;
    }

    public void setDiffprocess(double diffprocess) {

    }

    public double getInsert() {
        return 0;
    }

    public void setInsert(double insert) {

    }
}
