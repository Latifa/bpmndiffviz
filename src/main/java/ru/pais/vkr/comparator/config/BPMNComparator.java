package ru.pais.vkr.comparator.config;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.comparator.entities.MatchingPair;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public interface BPMNComparator {

    public MatchingPair compare(ComparisonResult comparisonResult,
                                BpmnModelInstance bpmnModelInstance1,
                                BpmnModelInstance bpmnModelInstance2,
                                ModelElementInstance modelElementInstance1,
                                ModelElementInstance modelElementInstance2);

    public double getDelete();

    public void setDelete(double delete);

    public double getDiffprocess();

    public void setDiffprocess(double diffprocess);

    public double getInsert();

    public void setInsert(double insert);

}
