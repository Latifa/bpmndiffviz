package ru.pais.vkr.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.pais.vkr.models.Document;
import ru.pais.vkr.models.Storage;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

@Repository
public class StorageDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Storage getAvailable() {
        return jdbcTemplate.queryForObject("SELECT * FROM storage WHERE is_available is TRUE LIMIT 1",
                new StorageMapper());
    }

    public void saveStorage(Storage storage) {
        jdbcTemplate.update("INSERT INTO storage (is_available, path) VALUES (?, ?)", storage.isAvailable(), storage.getPath());
    }

    public void updateStorage(Storage storage) {
        jdbcTemplate.update("UPDATE storage SET is_available = ?, path = ? WHERE id = ?", storage.isAvailable(), storage.getPath(), storage.getId());
    }

    public void saveOrUpdateStorage(Storage storage) {
        if (storage.getId() == 0) {
            saveStorage(storage);
        } else {
            updateStorage(storage);
        }
    }

}

final class StorageMapper implements RowMapper<Storage> {

    @Override
    public Storage mapRow(ResultSet resultSet, int i) throws SQLException {
        Storage storage = new Storage();
        storage.setId(resultSet.getLong(1));
        storage.setAvailable(resultSet.getBoolean(2));
        storage.setPath(resultSet.getString(3));
        return storage;
    }
}
