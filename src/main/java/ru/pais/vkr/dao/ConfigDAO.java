package ru.pais.vkr.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.pais.vkr.comparator.config.BPMNComparator;
import ru.pais.vkr.comparator.config.BaseConfig;
import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.comparator.entities.MatchingPair;
import ru.pais.vkr.comparator.entities.WrapperConfig;
import ru.pais.vkr.models.Document;

import java.sql.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

@Repository
public class ConfigDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public void saveConfigs(List<BPMNComparator> list, Long result_id) {
        for (BPMNComparator bpmnComparator : list) {
            saveConfig(bpmnComparator, result_id);
        }
    }

    public void saveConfig(BPMNComparator bpmnComparator, Long result_id) {
        jdbcTemplate.update("INSERT INTO " + bpmnComparator.getClass().getSimpleName().toLowerCase().substring(0,
                        bpmnComparator.getClass().getSimpleName().length() - 6) +
                        " (ins, del, diff, result_id) VALUES (?, ?, ?, ?)", bpmnComparator.getInsert(), bpmnComparator.getDelete(),
                bpmnComparator.getDiffprocess(), result_id);
    }

    public void updateConfigs(ComparisonResult comparisonResult) throws IllegalAccessException {
        List<BPMNComparator> bpmnComparators = comparisonResult.getCostConfig().getAllConfigs();
        for (BPMNComparator bpmnComparator : bpmnComparators) {
            updateConfigByID(bpmnComparator, comparisonResult.getId());
        }
    }

    public void updateConfigByID(BPMNComparator bpmnComparator, Long result_id) {
        BPMNComparator bpmnComparator1 = jdbcTemplate.queryForObject("SELECT * FROM " +
                        bpmnComparator.getClass().getSimpleName().toLowerCase().substring(0,
                                bpmnComparator.getClass().getSimpleName().length() - 6)
                        + " WHERE result_id = ?",
                new ConfigMapper(), result_id);
        bpmnComparator.setDiffprocess(bpmnComparator1.getDiffprocess());
        bpmnComparator.setInsert(bpmnComparator1.getInsert());
        bpmnComparator.setDelete(bpmnComparator1.getDelete());
    }
}

final class ConfigMapper implements RowMapper<BPMNComparator> {

    @Override
    public BPMNComparator mapRow(ResultSet resultSet, int i) throws SQLException {
        WrapperConfig wrapperConfig = new WrapperConfig();
        wrapperConfig.setInsert(resultSet.getDouble(2));
        wrapperConfig.setDelete(resultSet.getDouble(3));
        wrapperConfig.setDiffprocess(resultSet.getDouble(4));
        return wrapperConfig;
    }
}

