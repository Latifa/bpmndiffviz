package ru.pais.vkr.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.comparator.entities.MatchingPair;
import ru.pais.vkr.models.Document;

import java.sql.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

@Repository
public class ResultDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public void saveResult(final ComparisonResult comparisonResult) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection)
                    throws SQLException {
                PreparedStatement ps = connection.prepareStatement("INSERT INTO result (rdate, rec_counter, penalty, model_id_1, model_id_2, factor_levenshtein) VALUES (?, ?, ?, ?, ?, ?)", new String[]{"id"});
                ps.setTimestamp(1, new Timestamp(comparisonResult.getDate().getTime()));
                ps.setInt(2, comparisonResult.getRecCounter());
                ps.setDouble(3, comparisonResult.getPenalty());
                ps.setLong(4, comparisonResult.getFirstID());
                ps.setLong(5, comparisonResult.getSecondID());
                ps.setDouble(6, comparisonResult.getFactorLevenshtein());
                return ps;
            }
        }, holder);
        comparisonResult.setId((Long) holder.getKey());
    }

    public ComparisonResult getResultByID(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM result WHERE id = ?",
                new ComparisonResultMapper(), id);
    }

    public List<ComparisonResult> getAllResults() {
        return jdbcTemplate.query("SELECT * FROM result",
                new ComparisonResultMapper());
    }
}

final class ComparisonResultMapper implements RowMapper<ComparisonResult> {

    @Override
    public ComparisonResult mapRow(ResultSet resultSet, int i) throws SQLException {
        ComparisonResult comparisonResult = new ComparisonResult();
        comparisonResult.setId(resultSet.getLong(1));
        comparisonResult.setDate(resultSet.getTimestamp(2));
        comparisonResult.setRecCounter(resultSet.getInt(3));
        comparisonResult.setPenalty(resultSet.getDouble(4));
        comparisonResult.setFirstID(resultSet.getLong(5));
        comparisonResult.setSecondID(resultSet.getLong(6));
        comparisonResult.setFactorLevenshtein(resultSet.getDouble(7));
        return comparisonResult;
    }
}


