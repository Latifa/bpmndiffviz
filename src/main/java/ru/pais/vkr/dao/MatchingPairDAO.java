package ru.pais.vkr.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.pais.vkr.comparator.entities.MatchingPair;
import ru.pais.vkr.models.Document;
import ru.pais.vkr.models.Storage;

import java.sql.*;
import java.util.HashSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

@Repository
public class MatchingPairDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public HashSet<MatchingPair> getMatchingPairsMatchByID(Long result_id) {
        List<MatchingPair> matchingPairs = jdbcTemplate.query("SELECT * FROM matching_pair_match WHERE result_id = ?",
                new MatchingPairMapper(), result_id);
        return new HashSet(matchingPairs);
    }

    public HashSet<MatchingPair> getMatchingPairsDeleteByID(Long result_id) {
        List<MatchingPair> matchingPairs = jdbcTemplate.query("SELECT * FROM matching_pair_delete WHERE result_id = ?",
                new MatchingPairMapper(), result_id);
        return new HashSet(matchingPairs);
    }

    public HashSet<MatchingPair> getMatchingPairsInsertByID(Long result_id) {
        List<MatchingPair> matchingPairs = jdbcTemplate.query("SELECT * FROM matching_pair_add WHERE result_id = ?",
                new MatchingPairMapper(), result_id);
        return new HashSet(matchingPairs);
    }

    public void saveMatchingPairMatch(MatchingPair matchingPair, Long result_id) {
        jdbcTemplate.update("INSERT INTO matching_pair_match (first, second, definition, different_container, " +
                        "different_label, result_id) VALUES (?, ?, ?, ?, ?, ?)", matchingPair.getFirst().getAttributeValue("id"),
                matchingPair.getSecond().getAttributeValue("id"), matchingPair.getDefinition(), matchingPair.getDifferentContainer(),
                matchingPair.getDifferentLabel(), result_id);
    }

    public void saveMatchingPairInsert(MatchingPair matchingPair, Long result_id) {
        jdbcTemplate.update("INSERT INTO matching_pair_add (first, second, definition, different_container, " +
                        "different_label, result_id) VALUES (?, ?, ?, ?, ?, ?)", matchingPair.getFirst().getAttributeValue("id"),
                null, matchingPair.getDefinition(), matchingPair.getDifferentContainer(),
                matchingPair.getDifferentLabel(), result_id);
    }

    public void saveMatchingPairDelete(MatchingPair matchingPair, Long result_id) {
        jdbcTemplate.update("INSERT INTO matching_pair_delete (first, second, definition, different_container, " +
                        "different_label, result_id) VALUES (?, ?, ?, ?, ?, ?)", matchingPair.getFirst().getAttributeValue("id"),
                null, matchingPair.getDefinition(), matchingPair.getDifferentContainer(),
                matchingPair.getDifferentLabel(), result_id);
    }

}

final class MatchingPairMapper implements RowMapper<MatchingPair> {

    @Override
    public MatchingPair mapRow(ResultSet resultSet, int i) throws SQLException {
        MatchingPair matchingPair = new MatchingPair();
        matchingPair.setFirstID(resultSet.getString(2));
        matchingPair.setSecondID(resultSet.getString(3));
        matchingPair.setDefinition(resultSet.getString(4));
        matchingPair.setDifferentContainer(resultSet.getDouble(5));
        matchingPair.setDifferentLabel(resultSet.getDouble(6));
        return matchingPair;
    }
}

