package ru.pais.vkr.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.pais.vkr.models.Document;
import ru.pais.vkr.models.Model;
import ru.pais.vkr.models.Storage;

import java.sql.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
@Repository
public class ModelDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    DocumentDAO documentDAO;

    public Model getModelByID(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM model AS M JOIN document AS D ON M.document_id = D.id JOIN storage AS S ON D.storage_id = S.id WHERE M.id = ?",
                new ModelMapper(), id);
    }

    public void saveModel(final Model model) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection)
                    throws SQLException {
                PreparedStatement ps = connection.prepareStatement("INSERT INTO model (name, document_id) VALUES (?, ?)", new String[] {"id"});
                ps.setString(1, model.getName());
                ps.setLong(2, model.getDocument().getId());
                return ps;
            }
        }, holder);
        model.setId((Long) holder.getKey());
    }

    public void updateModel(Model model) {
        documentDAO.saveOrUpdateDocument(model.getDocument());
        jdbcTemplate.update("UPDATE model SET name = ?, document_id = ? WHERE id = ?", model.getName(), model.getDocument().getId(), model.getId());
    }

    public List<Model> getAllModels() {
        return jdbcTemplate.query("SELECT * FROM model AS M JOIN document AS D ON M.document_id = D.id JOIN storage AS S ON D.storage_id = S.id", new ModelMapper());
    }

    public void saveOrUpdateModel(Model model) {
        if (model.getId() == 0) {
            saveModel(model);
        } else {
            updateModel(model);
        }
    }


}

final class ModelMapper implements RowMapper<Model> {

    public Model mapRow(ResultSet resultSet, int i) throws SQLException {
        Model model = new Model();
        model.setId(resultSet.getLong(1));
        model.setName(resultSet.getString(2));
        Document document = new Document();
        document.setId(resultSet.getLong(4));
        document.setUploadDate(resultSet.getDate(5));
        document.setName(resultSet.getString(6));
        document.setSystemName(resultSet.getString(7));
        Storage storage = new Storage();
        storage.setId(resultSet.getLong(9));
        storage.setAvailable(resultSet.getBoolean(10));
        storage.setPath(resultSet.getString(11));
        document.setStorage(storage);
        model.setDocument(document);
        return model;
    }
}
