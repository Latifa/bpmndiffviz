package ru.pais.vkr.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.pais.vkr.models.Document;
import ru.pais.vkr.models.Model;
import ru.pais.vkr.models.Storage;

import javax.sql.DataSource;
import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */

@Repository
public class DocumentDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Document getDocumentByID(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM document AS D JOIN storage AS S ON D.storage_id = S.id WHERE D.id = ?",
                new DocumentMapper(), id);
    }

    public void saveDocument(final Document document) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection)
                    throws SQLException {
                PreparedStatement ps = connection.prepareStatement("INSERT INTO document (uploadDate, name, system_name, storage_id) VALUES (?, ?, ?, ?)", new String[] {"id"});
                ps.setDate(1, new Date(document.getUploadDate().getTime()));
                ps.setString(2, document.getName());
                ps.setString(3, document.getSystemName());
                ps.setLong(4, document.getStorage().getId());
                return ps;
            }
        }, holder);
        document.setId((Long) holder.getKey());
    }

    public void updateDocument(Document document) {
        jdbcTemplate.update("UPDATE document SET uploadDate = ?, name = ?, system_name = ?, storage_id = ? WHERE id = ? ", document.getUploadDate(), document.getName(), document.getSystemName(), document.getStorage().getId(), document.getId());
    }

    public void saveOrUpdateDocument(Document document) {
        if (document.getId() == 0) {
            saveDocument(document);
        } else {
            updateDocument(document);
        }
    }

}

final class DocumentMapper implements RowMapper<Document> {

    @Override
    public Document mapRow(ResultSet resultSet, int i) throws SQLException {
        Document document = new Document();
        document.setId(resultSet.getLong(1));
        document.setUploadDate(resultSet.getDate(2));
        document.setName(resultSet.getString(3));
        document.setSystemName(resultSet.getString(4));
        Storage storage = new Storage();
        storage.setId(resultSet.getLong(6));
        storage.setAvailable(resultSet.getBoolean(7));
        storage.setPath(resultSet.getString(8));
        document.setStorage(storage);
        return document;
    }
}
