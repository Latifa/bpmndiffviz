package ru.pais.vkr.test;

import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.*;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.camunda.bpm.model.xml.instance.DomElement;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.pais.vkr.comparator.entities.ComparisonResult;
import ru.pais.vkr.comparator.entities.CostConfig;
import ru.pais.vkr.services.CompareService;
import ru.pais.vkr.utils.FileManager;
import ru.pais.vkr.utils.StringUtil;

import java.io.File;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Ivanov Sergey
 */
public class Test1 {

    @Test
    public void test1() throws Exception {
        String instance1 = "src/test/resources/test1/123.bpmn";
        String instance2 = "src/test/resources/test1/321.bpmn";
        CostConfig costConfig = new CostConfig();
        CompareService compareService = new CompareService();
        ComparisonResult comparisonResult =  compareService.compareModels(instance1, instance2, costConfig);
        Assert.assertEquals(7, comparisonResult.getRecCounter());
        Assert.assertTrue(Double.compare(18d, comparisonResult.getPenalty()) == 0);
        Assert.assertEquals(5, comparisonResult.getHeuristic());
        Assert.assertEquals(4, comparisonResult.getMatchingSet().size());
        Assert.assertEquals(2, comparisonResult.getAddedSet().size());
        Assert.assertEquals(0, comparisonResult.getDeletedSet().size());
    }

    @Ignore
    @Test
    public void test2() throws Exception {
        String instance1 = "src/test/resources/test2/Get car 1.bpmn";
        String instance2 = "src/test/resources/test2/Get car 2.bpmn";
        CostConfig costConfig = new CostConfig();
        CompareService compareService = new CompareService();
        ComparisonResult comparisonResult =  compareService.compareModels(instance1, instance2, costConfig);
        Assert.assertEquals(38, comparisonResult.getRecCounter());
        Assert.assertTrue(Double.compare(117d, comparisonResult.getPenalty()) == 0);
        Assert.assertEquals(14, comparisonResult.getHeuristic());
        Assert.assertEquals(15, comparisonResult.getMatchingSet().size());
        Assert.assertEquals(4, comparisonResult.getAddedSet().size());
        Assert.assertEquals(18, comparisonResult.getDeletedSet().size());
    }

    @Ignore
    @Test
    public void test3() throws Exception {
        String instance1 = "src/test/resources/test3/inductive_miner 1.bpmn";
        String instance2 = "src/test/resources/test3/inductive_miner 2.bpmn";
        CostConfig costConfig = new CostConfig();
        CompareService compareService = new CompareService();
        ComparisonResult comparisonResult =  compareService.compareModels(instance1, instance2, costConfig);
        Assert.assertEquals(19, comparisonResult.getRecCounter());
        Assert.assertEquals(54d, comparisonResult.getPenalty(), 0.001);
        Assert.assertEquals(2, comparisonResult.getHeuristic());
        Assert.assertEquals(14, comparisonResult.getMatchingSet().size());
        Assert.assertEquals(1, comparisonResult.getAddedSet().size());
        Assert.assertEquals(3, comparisonResult.getDeletedSet().size());
    }

    @Ignore
    @Test
    public void test4() throws Exception {
        String instance1 = "src/test/resources/test4/Scan Aztec-code.bpmn";
        String instance2 = "src/test/resources/test4/Scan QR-code.bpmn";
        CostConfig costConfig = new CostConfig();
        CompareService compareService = new CompareService();
        ComparisonResult comparisonResult =  compareService.compareModels(instance1, instance2, costConfig);
        Assert.assertEquals(19, comparisonResult.getRecCounter());
        Assert.assertTrue(Double.compare(54d, comparisonResult.getPenalty()) == 0);
        Assert.assertEquals(2, comparisonResult.getHeuristic());
        Assert.assertEquals(14, comparisonResult.getMatchingSet().size());
        Assert.assertEquals(1, comparisonResult.getAddedSet().size());
        Assert.assertEquals(3, comparisonResult.getDeletedSet().size());
    }

}
